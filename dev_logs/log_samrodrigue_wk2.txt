NAME: Sam Rodrigue
DATE: 26/09/2016 - 2/10/2016
HOURS: 3hrs

MEETINGS:
None

DUTIES AND ACHIEVMENTS
1. Developed a list of high level major task that are required for the project.
2. Prepared a flow chart to display the dependencies between the major tasks and a table to assign each major task to a group member.
