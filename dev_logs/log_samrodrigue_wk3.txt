NAME: Sam Rodrigue
DATE: 3/10/2016 - 9/10/2016
HOURS: 4hrs

MEETINGS:
6/10/2016 - 1.5hrs

DUTIES AND ACHIEVMENTS
1. Further developed list of major tasks with help from team members.
2. Divided majors tasks to individual team members with assigned supporting team members.
 - Assigned the task of UI/Menu design and game state.
3. Began preliminary work on UML class diagram for UI/Menu and game state major tasks.
