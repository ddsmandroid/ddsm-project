NAME: Sam Rodrigue
DATE: 31/10/2016 - 6/11/2016
HOURS: 5hrs

MEETINGS:
1.5hrs - informal meeting/remote group work

DUTIES AND ACHIEVMENTS
1. Developed additional game state called menu which is the welcome screen that allows the user to start a game or leave the application
2. Created button class that will can be pushed and display text
3. Set constants for game update rate and game draw rate. 
	Note: update and draw rate seem appropriate for the time being. Will need to be changes if update and drawing requirements change
4. Started work on functions that can be used to scale the game from map size (1440) to phone size (can vary). This will allow use to properly display the game on multiple phones.