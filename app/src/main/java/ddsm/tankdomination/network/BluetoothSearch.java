package ddsm.tankdomination.network;

import android.bluetooth.*;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.LinkedList;
import java.util.Set;

/**
 * This is the Bluetooth implementation of the Network Controller Abstract Class
 *
 * @author David Farrar
 * @version 1.0
 * @since 2016-11-13
 */

public class BluetoothSearch {

    public static void enableDiscoverable(Context context){
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 600);
        context.startActivity(discoverableIntent);
        Log.i("Function Logger","Device Ready");
    }
}
