package ddsm.tankdomination.network;

import android.bluetooth.*;

import java.io.*;

/**
 * Created by david on 2016-11-14.
 */

public class BluetoothThread extends Thread {
    private final BluetoothSocket socket;
    private final BluetoothController controller;
    private final InputStream inStream;
    private final OutputStream outStream;
    private BluetoothHandler handler;


    public BluetoothThread(BluetoothSocket bSocket, BluetoothController bController, BluetoothHandler handler) {
        controller = bController;
        this.handler = handler;
        socket = bSocket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) { }

        inStream = tmpIn;
        outStream = tmpOut;
    }

    public void run() {
        byte[] buffer = new byte[1024];  // buffer store for the stream
        int bytes; // bytes returned from read()

        // Keep listening to the InputStream until an exception occurs
        while (true) {
            try {
                bytes = inStream.read(buffer);
                handler.obtainMessage(handler.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
            } catch (IOException e) {
                break;
            }
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            outStream.write(bytes);
        } catch (IOException e) { }
    }

    /* Call this from the main activity to shutdown the connection */
    public void cancel() {
        try {
            socket.close();
        } catch (IOException e) { }
    }
}