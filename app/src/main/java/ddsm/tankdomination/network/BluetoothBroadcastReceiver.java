package ddsm.tankdomination.network;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.util.LinkedList;

import ddsm.tankdomination.GameActivity;

/**
 * Created by David on 2016-11-24.
 */

class BluetoothBroadcastReceiver extends BroadcastReceiver{
    private GameActivity activity;
    private BluetoothController controller;

    public BluetoothBroadcastReceiver(GameActivity activity, BluetoothController controller){
        super();
        this.activity = activity;
        this.controller = controller;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Function Query","Device Receive Message");
        String action = intent.getAction();

        if(BluetoothDevice.ACTION_FOUND.equals(action)){

            Log.i("Function Query","Device Queried");
            // Get the BluetoothDevice object from the Intent
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            // Add the name and address to an array adapter to show in a ListView
            Log.i("Function Device", device.getName() + "\n" + device.getAddress());
            controller.addDevice(device);
        }
    }



    public void register(){
        Log.i("Function Register","Register Reciever");
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        activity.getContext().registerReceiver(this, filter);

    }

    public void unregister(){
        Log.i("Function Unregister","Unregister Reciever");
        try{
            activity.getContext().unregisterReceiver(this);
        }
        catch(IllegalArgumentException e){
            Log.i("Function Error:", "Illegal Argument Exception: " +e.toString());
        }
    }
}