package ddsm.tankdomination.network;

import android.os.Message;

import android.os.Handler;
import java.util.logging.LogRecord;

/**
 * Created by david on 2016-11-30.
 */

public class BluetoothHandler extends Handler{
    BluetoothController controller;

    public static int MESSAGE_READ = 1;

    public BluetoothHandler(BluetoothController controller){
        super();
        this.controller = controller;
    }

    public void handleMessage(Message msg){
        if(msg.what==MESSAGE_READ){
            byte[] readBuf = (byte[])msg.obj;
            controller.parseData(readBuf);
        }
    }
}
