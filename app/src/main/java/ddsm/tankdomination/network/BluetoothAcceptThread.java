package ddsm.tankdomination.network;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class BluetoothAcceptThread extends Thread {
    private final BluetoothServerSocket mmServerSocket;
    BluetoothAdapter adapter;
    BluetoothController controller;
    UUID uuid;

    public BluetoothAcceptThread(BluetoothController cntrl, BluetoothAdapter adptr, UUID uid) {
        // Use a temporary object that is later assigned to mmServerSocket,
        // because mmServerSocket is final
        adapter = adptr;
        controller = cntrl;
        uuid = uid;

        BluetoothServerSocket tmp = null;
        try {
            // MY_UUID is the app's UUID string, also used by the client code
            tmp = adapter.listenUsingRfcommWithServiceRecord("tank_domination", uuid);
        } catch (IOException e) { }
        mmServerSocket = tmp;
    }

    public void run() {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned
        while (true) {
            try {
                socket = mmServerSocket.accept();
            } catch (IOException e) {
                break;
            }
            // If a connection was accepted
            if (socket != null) {
                Log.i("Function Accept", "accept thread");
                // Do work to manage the connection (in a separate thread)
                controller.connectSocket(socket);
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    /** Will cancel the listening socket, and cause the thread to finish */
    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) { }
    }
}