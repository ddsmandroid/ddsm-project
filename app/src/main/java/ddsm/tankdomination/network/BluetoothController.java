package ddsm.tankdomination.network;

import android.app.AlertDialog;
import android.bluetooth.*;
import android.content.*;
import android.util.Log;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.HitMessage;
import ddsm.tankdomination.model.Model;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.Tank;
import ddsm.tankdomination.view.PlayerObject;

import static ddsm.tankdomination.GameStateType.GAME;
import static ddsm.tankdomination.GameStateType.MENU;

/**
 * This is the Bluetooth implementation of the Network Controller Abstract Class
 *
 * @author David Farrar
 * @version 1.0
 * @since 2016-11-13
 */

/**
 * Bluetooth ID Codes
 * bull - bullet data 20 bytes
 * tank - tank data
 * hitt - hit data 8 bytes
 */

public class BluetoothController{
    private BluetoothAdapter bluetooth;
    private LinkedList<BluetoothThread> threads;
    public boolean connected = false;
    private BluetoothBroadcastReceiver reciever;
    private int targetPlayers;
    private LinkedList<BluetoothDevice> devices;
    public BluetoothHandler handler;
    private Boolean ready = false;
    private int timeSinceLastMessage = 0;

    private UUID uuid = UUID.fromString("b4dac5e0-ab18-11e6-9598-0800200c9a66");

    public GameActivity activity;

    private Model model;
    public TreeMap<String,PlayerObject> players;

    public BluetoothController(GameActivity a, Model mdl){
        model = mdl;
        activity = a;
        targetPlayers = 2;
        threads = new LinkedList<>();
        bluetooth = BluetoothAdapter.getDefaultAdapter();
        handler = new BluetoothHandler(this);
        devices = new LinkedList<>();
        reciever = new BluetoothBroadcastReceiver(activity, this);
    }

    /**
     * Starts bluetooth adapter
     * Should be run at start of app
     */
    public void startAdapter() {
        Log.i("Function:StartAdapter", "starting adapter");

        if(bluetooth == null){
            // Device does not support Bluetooth'
            Log.i("Function:Error", "device does not support Bluetooth");
            // Device does not support Bluetooth
            // Display Error Message For Devices that don't have bluetooth
            new AlertDialog.Builder(activity.getContext())
                    .setTitle("Bluetooth Required")
                    .setMessage("Bluetooth is required for the multiplayer component of Tank Domination.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Return User to Main Menu
                            activity.setGameState(MENU);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        if(!bluetooth.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.getContext().startActivity(enableBtIntent);
        }
    }

    public void discoverBondedDevices(){
        Log.i("Function: Discovering", "adding paired devices");
        Set<BluetoothDevice> bonded = bluetooth.getBondedDevices();
        Object[] devices = bonded.toArray();
        for(Object device: devices){
            addDevice((BluetoothDevice) device);
        }
    }

    public void discoverDevices(){
        reciever.register();
        bluetooth.startDiscovery();
    }

    public Boolean startServer() {
        Log.i("Function:Start Server", "starting server");
        BluetoothSearch.enableDiscoverable(activity.getContext());
        bluetooth.startDiscovery();
        BluetoothAcceptThread acceptThread = new BluetoothAcceptThread(this, bluetooth, uuid);
        acceptThread.start();
        return true;
    }

    public Boolean connectUser(String macAddress){
        Log.i("Function:Connect User", "MAC Address: "+ macAddress);
        BluetoothConnectThread connectThread = new BluetoothConnectThread(this, bluetooth.getRemoteDevice(macAddress), bluetooth, uuid);
        connectThread.start();
        return true;
    }

    public void connectSocket(BluetoothSocket socket){
        Log.i("Function: Connecting", "connecting socket");
        BluetoothThread thread = new BluetoothThread(socket,this, handler);
        bluetooth.cancelDiscovery();
        connected = true;
        threads.add(thread);
        thread.start();
    }

    public Boolean sendTank(Tank t) {
        Log.i("Function:Send Tank", "oid: "+t.getOwnerId()+",tid: "+t.getTankId()+", x:"+t.getPosition().x+", y:"+t.getPosition().y);
        byte[] message = BluetoothParser.bytesFromTank(t);
        for(BluetoothThread thr : threads){
            thr.write(message);
        }
        return true;
    }

    public Boolean sendBullet(Bullet b) {
        Log.i("Function:Send Bullet", "oid: "+b.getOwnerId()+",bid: "+b.getBulletId()+", x:"+b.getPosition().x+", y:"+b.getPosition().y);
        byte[] message = BluetoothParser.bytesFromBullet(b);
        for(BluetoothThread thr : threads){
            thr.write(message);
        }
        return true;
    }

    public Boolean sendHit(String type, String id, String ownerId) {
        Log.i("Function:Send Hit", "Object Type: " +type+ ",id: "+id+ ",oid: " + ownerId);
        model.getPlayer(ownerId).getTank().setAlive(false);
        byte[] message = BluetoothParser.bytesFromHitCode(type, id, ownerId);
        for(BluetoothThread thr : threads){
            thr.write(message);
        }
        return true;
    }

    public Boolean sendPlayer() {
        Log.i("Function:Send Player", "Player" + model.getPlayer(model.getUserId()).toString());
        byte[] message = BluetoothParser.bytesFromPlayer(model.getPlayer(model.getUserId()));
        for(BluetoothThread thr : threads){
            thr.write(message);
        }
        return true;
    }

    public Boolean sendGameStart(){
        Log.i("Function:Game Start", "sending game start");
        ready = true;
        byte[] message = BluetoothParser.gameStartMessage();
        for(BluetoothThread thr : threads){
            thr.write(message);
        }
        return true;
    }

    public Boolean sendEndGame() {
        Log.i("Function: End Game", "sending end game");
        byte[] message = BluetoothParser.gameEndMessage();
        for(BluetoothThread thr : threads){
            thr.write(message);
        }
        return true;
    }

    public LinkedList<BluetoothDevice> getDevices() {
        if(!devices.isEmpty()){
            return devices;
        }
        else{
            return new LinkedList<>();
        }
    }

    public Boolean parseData(byte[] buffer) {
        Log.i("Function:Parseing", "parseing buffer from network, Buffer: " + new String(buffer));
        String code = BluetoothParser.codeFromBytes(buffer);

        Log.i("Function:Parseing", "parseing code:: " + code);
        if(code.equals("bull")){
            Bullet b = BluetoothParser.bulletFromBytes(buffer);
            model.getPlayer(b.getOwnerId()).addBullet(b);
            timeSinceLastMessage = 0;
        }
        else if(code.equals("tank")){
            Tank t = BluetoothParser.tankFromBytes(buffer);
            Player p = model.getPlayer(t.getOwnerId());
            if(p!=null){
                p.updateTank(t);
            }
            else{
            }
            timeSinceLastMessage = 0;
        }
        else if(code.equals("hitt")){
            HitMessage message = BluetoothParser.hitCodeFromByte(buffer);
            model.getPlayer(message.ownerId).getTank().setAlive(false);
            timeSinceLastMessage = 0;
        }
        else if(code.equals("strt")){
            ready = true;
            timeSinceLastMessage = 0;
        }
        else if(code.equals("plyr")){
            Player player = BluetoothParser.playerFromBytes(buffer);
            model.addPlayer(player);
            timeSinceLastMessage = 0;
        }
        else if(code.equals("endg")){
            activity.getGameState().playing = false;
            timeSinceLastMessage = 0;
        }

        return true;
    }

    public void close(){
        reciever.unregister();
    }

    public void setTargetPlayers(int targetPlayers){
        this.targetPlayers = targetPlayers;
    }

    public int getTargetPlayers() {
        return targetPlayers;
    }

    public void addDevice(BluetoothDevice device){
        for(BluetoothDevice d: devices){
            if(d.getAddress().equals(device.getAddress())){
                return;
            }
        }
        devices.add(device);
    }

    public void updateState() {
        Log.i("Function: UpdateControl", "Connnected: " + connected + ", GameSate: " + activity.getGameState());
        if(connected){
            sendPlayer();
        }
        if(activity.model.getNumPlayers()==getTargetPlayers()){
            connected = false;
            sendGameStart();
        }
        if(ready){
            activity.setGameState(GAME);
        }
    }
    public void update(){
        Log.i("Function: Update", "Time: " + timeSinceLastMessage + ", GameSate: " + activity.getGameState());
        if(timeSinceLastMessage>200){
            connected = false;
            activity.setGameState(MENU);
        }
        timeSinceLastMessage++;
    }
}
