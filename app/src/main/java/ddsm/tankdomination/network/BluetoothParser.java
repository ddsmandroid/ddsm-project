package ddsm.tankdomination.network;

import android.util.Log;

import java.nio.ByteBuffer;

import ddsm.tankdomination.model.*;

/**
 * Created by david on 2016-11-14.
 */

public class BluetoothParser {
    public static String codeFromBytes(byte[] b){
        return BluetoothParser.stringFromBytes(new byte[]{b[0],b[1],b[2],b[3]});
    }

    public static byte[] bytesFromBullet(Bullet b){
        int i = 0;
        String id = b.getBulletId();
        float angle = b.getAngle();
        Position pos = b.getPosition();
        String name= b.getOwnerId();
        if(name.length()>4){
            name = name.substring(0,4);
        }

        byte[] s = BluetoothParser.byteFromString("bull"); // 4bytes
        byte[] d = BluetoothParser.byteFromString(id); //4bytes
        byte[] x = BluetoothParser.byteFromInteger((int)pos.x); // 4bytes
        byte[] y = BluetoothParser.byteFromInteger((int)pos.y); // 4bytes
        byte[] a = BluetoothParser.byteFromInteger((int)angle); // 4bytes
        byte[] n = BluetoothParser.byteFromString(name); // 4bytes;

        byte[] message = new byte[24];
        for(byte m: s){
            message[i] = m;
            i++;
        }
        for(byte m: d){
            message[i] = m;
            i++;
        }
        for(byte m: x) {
            message[i] = m;
            i++;
        }
        for(byte m: y) {
            message[i] = m;
            i++;
        }
        for(byte m: a) {
            message[i] = m;
            i++;
        }
        for(byte m: n) {
            message[i] = m;
            i++;
        }
        return message;
    }

    public static Bullet bulletFromBytes(byte[] b){
        String id = stringFromBytes(new byte[]{b[4],b[5],b[6],b[7]});
        int x = intFromBytes(new byte[]{b[8],b[9],b[10],b[11]});
        int y = intFromBytes(new byte[]{b[12],b[13],b[14],b[15]});
        int angle = intFromBytes(new byte[]{b[16],b[17],b[18],b[19]});
        String name = stringFromBytes(new byte[]{b[20],b[21],b[22],b[23]});
        return new Bullet(id, name, new Position(x,y), (float)angle, PlayerColor.RED);
    }

    public static byte[] bytesFromTank(Tank t){
        int i = 0;
        String id = t.getTankId();
        int xInt = (int)t.getPosition().x;
        int yInt = (int)t.getPosition().y;
        int hInt = (int)t.getMoveState().h;
        int vInt = (int)t.getMoveState().v;
        int angle = (int)t.getMoveState().angle;
        int tAngle = (int)t.getTurretAngle();
        String oid = t.getOwnerId();
        if(oid.length()>4){
            oid = oid.substring(0,4);
        }

        byte[] s = BluetoothParser.byteFromString("tank"); // 4bytes
        byte[] d = BluetoothParser.byteFromString(id); //4bytes
        byte[] x = BluetoothParser.byteFromInteger(xInt); // 4bytes
        byte[] y = BluetoothParser.byteFromInteger(yInt); // 4bytes
        byte[] h = BluetoothParser.byteFromInteger(hInt); // 4bytes
        byte[] v = BluetoothParser.byteFromInteger(vInt); // 4bytes
        byte[] a = BluetoothParser.byteFromInteger(angle); // 4bytes
        byte[] at = BluetoothParser.byteFromInteger(tAngle); // 4bytes
        byte[] n = BluetoothParser.byteFromString(oid); // 4bytes;

        byte[] message = new byte[36];
        for(byte m: s){
            message[i] = m;
            i++;
        }
        for(byte m: d){
            message[i] = m;
            i++;
        }
        for(byte m: x) {
            message[i] = m;
            i++;
        }
        for(byte m: y) {
            message[i] = m;
            i++;
        }
        for(byte m: h) {
            message[i] = m;
            i++;
        }
        for(byte m: v) {
            message[i] = m;
            i++;
        }
        for(byte m: a) {
            message[i] = m;
            i++;
        }
        for(byte m: at) {
            message[i] = m;
            i++;
        }
        for(byte m: n) {
            message[i] = m;
            i++;
        }
        return message;
    }

    public static Tank tankFromBytes(byte[] b){
        String id = stringFromBytes(new byte[]{b[4],b[5],b[6],b[7]});
        int x = intFromBytes(new byte[]{b[8],b[9],b[10],b[11]});
        int y = intFromBytes(new byte[]{b[12],b[13],b[14],b[15]});
        int h = intFromBytes(new byte[]{b[16],b[17],b[18],b[19]});
        int v = intFromBytes(new byte[]{b[20],b[21],b[22],b[23]});
        int angle = intFromBytes(new byte[]{b[24],b[25],b[26],b[27]});
        int tAngle = intFromBytes(new byte[]{b[28],b[29],b[30],b[31]});
        String name = stringFromBytes(new byte[]{b[32],b[33],b[34],b[35]});
        return new Tank(id,name,new Position(x,y),new MoveState(h,v,angle),tAngle);
    }

    public static byte[] bytesFromHitCode(String type, String code, String ownerId){
        byte[] message = new byte[16];
        int i = 0;

        byte[] a = BluetoothParser.byteFromString("hitt"); // 4bytes
        byte[] b = BluetoothParser.byteFromString(code); //4bytes
        byte[] c = BluetoothParser.byteFromString(type); //4bytes
        byte[] d = BluetoothParser.byteFromString(ownerId); //4bytes

        for(byte m: a){
            message[i] = m;
            i++;
        }
        for(byte m: b){
            message[i] = m;
            i++;
        }
        for(byte m: c){
            message[i] = m;
            i++;
        }
        for(byte m: d){
            message[i] = m;
            i++;
        }
        return message;
    }

    public static HitMessage hitCodeFromByte(byte[] b){
        String id = stringFromBytes(new byte[]{b[4],b[5],b[6],b[7]});
        String type = stringFromBytes(new byte[]{b[8],b[9],b[10],b[11]});
        String ownerId = stringFromBytes(new byte[]{b[12],b[13],b[14],b[15]});

        return new HitMessage(id, ownerId, type);
    }

    public static byte[] gameStartMessage() {
        byte[] message = new byte[4];
        int i = 0;

        byte[] a = BluetoothParser.byteFromString("strt"); // 8bytes

        for(byte m: a){
            message[i] = m;
            i++;
        }
        return message;
    }


    public static byte[] gameEndMessage() {

        byte[] message = new byte[4];
        int i = 0;

        byte[] a = BluetoothParser.byteFromString("endg"); // 8bytes

        for(byte m: a){
            message[i] = m;
            i++;
        }
        return message;
    }

    public static byte[] byteFromString(String s){
        byte[] b = s.getBytes();
        if(b.length==4){
            return b;
        }
        else{
            Log.i("b2",""+ new String(b));
            //byte[] b2 = {b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7]};
            //return b2;
        }
        return b;
    }

    public static String stringFromBytes(byte[] b){
        return new String(b);
    }

    public static byte[] byteFromInteger(Integer i){
        return ByteBuffer.allocate(4).putInt(i).array();
    }

    public static int intFromBytes(byte[] b){
        return ByteBuffer.wrap(b).getInt();
    }

    public static byte[] bytesFromPlayer(Player player) {
        byte[] message = new byte[16];
        int i = 0;

        byte[] a = BluetoothParser.byteFromString("plyr"); // 4bytes
        byte[] b = BluetoothParser.byteFromString(player.getUserId()); //4bytes
        byte[] c = BluetoothParser.byteFromString(player.getName()); //4bytes
        String color = "redd";
        switch(player.getColor()){
            case RED: color = "redd";
                break;
            case GREEN: color = "gren";
                break;
            case BLUE: color = "blue";
                break;
            case YELLOW: color = "yell";
                break;
        }
        byte[] d = BluetoothParser.byteFromString(color); //4bytes

        for(byte m: a){
            message[i] = m;
            i++;
        }
        for(byte m: b){
            message[i] = m;
            i++;
        }
        for(byte m: c){
            message[i] = m;
            i++;
        }
        for(byte m: d){
            message[i] = m;
            i++;
        }
        return message;
    }

    public static Player playerFromBytes(byte[] b) {
        String id = stringFromBytes(new byte[]{b[4],b[5],b[6],b[7]});
        String name = stringFromBytes(new byte[]{b[8],b[9],b[10],b[11]});
        String color = stringFromBytes(new byte[]{b[12],b[13],b[14],b[15]});
        PlayerColor plrColor = PlayerColor.BLUE;
        switch(color){
            case "redd": plrColor = PlayerColor.RED;
                break;
            case "blue": plrColor = PlayerColor.BLUE;
                break;
            case "yell": plrColor = PlayerColor.YELLOW;
                break;
            case "gren": plrColor = PlayerColor.GREEN;
                break;
        }

        return new Player(id, name, plrColor);
    }
}
