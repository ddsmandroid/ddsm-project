package ddsm.tankdomination;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import ddsm.tankdomination.model.*;
import ddsm.tankdomination.view.*;
import ddsm.tankdomination.network.*;

public class GameActivity extends SurfaceView implements Runnable {
    // Master debug boolean. This will keep debug code from being compiled unless needed
    public static final boolean debug = false;

    // State Thread
    Thread stateThread = null;

    public Context context;

    // SurfaceHolder to paint/canvas in the thread
    public SurfaceHolder holder;

    // Canvas and Paint object
    public Canvas canvas;
    public Paint paint;

    // A boolean which we will set and unset when game is running/not
    public volatile boolean playing;

    // Sound
    public boolean effectsOn    = true;
    public boolean musicOn      = true;

    // Store the size of the screen
    int screenHeight = 0;
    int screenWidth  = 0;
    public int mapWidth = 2560;
    public int mapHeight = 1440;
    float mapX;
    float mapY;
    float mapScale;

    // GameView State time
    long prevTime = System.currentTimeMillis();
    long drawTime = 0;
    long updateTime = 0;
    public float fps;

    // GameState will be current view/state of the game
    private GameState gameState;

    public Model model;
    public BluetoothController network;

    public GameActivity(Context cntxt) {
        super(cntxt);
        context = cntxt;
        holder = getHolder();
        setFocusable(true);

        // Set initial screen size
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        setMapScale(metrics.widthPixels, metrics.heightPixels);
        // Set GameActivity variables
        canvas = new Canvas();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        gameState = new Menu(this);

        String userId = Build.SERIAL;
        userId = userId.substring(userId.length()-4);
        userId = userId.replace('t','n');
        userId = userId.replace('b','o');
        model = new Model(userId, userId, PlayerColor.RED);

        network = new BluetoothController(this,model);
        network.startAdapter();

        playing = true;
    }

    protected void resume() {
        if( gameState.mediaPlayer != null )
            gameState.mediaPlayer.start();
        playing = true;
        stateThread = new Thread(this);
        stateThread.start();
    }

    protected void pause() {
        if( gameState.mediaPlayer != null && gameState.mediaPlayer.isPlaying() )
            gameState.mediaPlayer.pause();
        playing = false;
        try {
            stateThread.join();
        } catch ( InterruptedException e ) {
            Log.e("Error:", "joining thread" );
        }
    }

    public void setGameState(GameStateType gameStateType) {
        if( gameState.mediaPlayer != null && gameState.mediaPlayer.isPlaying() )
            gameState.mediaPlayer.stop();
        switch (gameStateType) {
            case MENU:
                gameState = new Menu(this);
                break;
            case GAME:
                gameState = new GameView(this);
                break;
            case CREATE:
                gameState = new CreateGameView(this);
                break;
            case JOIN:
                gameState = new JoinGameView(this);
                break;
            case TUTORIAL:
                gameState = new TutorialView(this);
                break;
            case SINGLE:
                gameState = new SinglePlayerView(this);
                break;
        }
    }

    // Normalizing function for different screen sizes
    public float normX(float x) { return (mapScale * x) + mapX; }
    public float normY(float y) { return (mapScale * y) + mapY; }
    public float norm(float a) { return mapScale * a; }
    public float mapNormX(float x) { return (x - mapX) / mapScale; }
    public float mapNormY(float y) { return (y - mapY) / mapScale; }
    public float mapNorm(float a) { return a / mapScale; }

    public Bitmap loadBitmap(int resourceID) {
        return BitmapFactory.decodeResource(getResources(), resourceID);
    }

    public Bitmap normBitmap(Bitmap source) {
        return Bitmap.createScaledBitmap(source, (int)norm(source.getWidth()), (int)norm(source.getHeight()), true);
    }

    @Override
    public void run() {
        while( playing ) {
            long currTime = System.currentTimeMillis();
            long deltaTime = currTime - prevTime;
            prevTime = currTime;
            drawTime += deltaTime;
            updateTime += deltaTime;

            // Limit update to 60 fps (16.67ms)(
            if (updateTime > 16) {
                // Update
                gameState.update(updateTime);
                // Reset update time
                updateTime = 0;
            }

            //Limit drawing to 30 fps (33.33ms)
            if (drawTime > 33) {
                fps = 1000 / drawTime;
                // Draw the frame
                gameState.draw();
                drawTime = 0;
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        // Calculate scale factors
        setMapScale(getWidth(), getHeight());
    }

    private void setMapScale(int sWidth, int sHeight) {
        screenWidth = sWidth;
        screenHeight = sHeight;
        float mapAspect = (float)mapWidth/(float)mapHeight;
        if (screenWidth > (float)screenHeight * mapAspect) {
            // Screen is too wide
            mapScale = (float)screenHeight / (float)mapHeight;
            mapX = ((float)screenWidth - (float)mapWidth * mapScale) / 2.f;
            mapY = 0.f;
        } else {
            // Screen is possibly too high
            mapScale = (float)screenWidth / (float)mapWidth;
            mapY = ((float)screenHeight - ((float)mapHeight * mapScale)) / 2.f;
            mapX = 0.f;
        }
    }

    public GameStateType getGameStateType() {
        return this.gameState.gameStateType;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //screenWidth = getWidth();
        //screenHeight = getHeight();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Pass touch events to GameState
        return gameState.touchEvent(event);
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    // Draw high contrast text
    public void drawText(String t, float x, float y, Paint p) {
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        canvas.drawText(t, x, y, p);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLACK);
        canvas.drawText(t, x, y, p);
    }

    public void exitGame(){
        network.close();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    public void drawText(String t, float x, float y, Paint p, float a) {
        int alpha = (int)Math.floor(255.f * a);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(alpha, 255, 255, 255));
        canvas.drawText(t, x, y, p);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.argb(alpha, 0, 0, 0));
        canvas.drawText(t, x, y, p);
    }

    public void resetTime() { prevTime = System.currentTimeMillis();}

    public void togglePause() { gameState.paused  = !gameState.paused; }
}
