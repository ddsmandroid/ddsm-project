package ddsm.tankdomination;

import android.media.MediaPlayer;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.view.Controller;
import ddsm.tankdomination.view.Map;
import ddsm.tankdomination.view.PlayerObject;

/**
 * Abstract GameState class
 */

public abstract class GameState {
    public abstract void update(long deltaTime);
    public abstract void draw();
    protected abstract boolean touchEvent(MotionEvent event);
    public abstract LinkedList<PlayerObject> getPlayersList();

    // Static attributes that are used by all GameView States
    public static GameActivity activity;
    public GameStateType gameStateType;
    public static Controller controller;
    public Map map;
    public TreeMap<String,PlayerObject> players;
    public boolean playing;
    public boolean paused;
    public MediaPlayer mediaPlayer;

    public abstract void sendBullet(Bullet bullet);
    public abstract void sendHit(String type, String id, String ownerId);
}
