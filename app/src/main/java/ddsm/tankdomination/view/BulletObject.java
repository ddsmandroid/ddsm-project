package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import ddsm.tankdomination.GameState;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.PlayerColor;

import static ddsm.tankdomination.GameState.activity;


/**
 * BulletObject class
 */

public class BulletObject {
    GameState game;
    Bullet bullet;
    Bitmap bulletMap;
    Matrix matrix;
    float angle = 0;

    // BulletObject constants
    float speed = 500.f; // Should be map pixels per second

    public BulletObject(GameState gs, Bullet blt) {
        game = gs;

        matrix = new Matrix();
        
        bullet = blt;
        setBitmapColor();
    }

    public void draw() {
        matrix.reset();
        matrix.postTranslate( -bulletMap.getWidth()/2.f, -bulletMap.getHeight()/2.f);
        matrix.postRotate( bullet.getAngle() );
        matrix.postTranslate( activity.normX(bullet.getPosition().x), activity.normY(bullet.getPosition().y) );
        game.activity.canvas.drawBitmap(bulletMap, matrix, null );
    }

    public void update( float dt ) {
        float moveSpeed = speed * (float) dt / 1000.f;
        bullet.getPosition().x += Math.cos( Math.toRadians( bullet.getAngle() ) ) * moveSpeed;
        bullet.getPosition().y += Math.sin( Math.toRadians( bullet.getAngle() ) ) * moveSpeed;
    }

    public Boolean offScreen() {
        // Outside of the screen
        if(bullet.getPosition().x < 0 || bullet.getPosition().x > activity.mapWidth )
            return true;

        if(bullet.getPosition().y < 0 || bullet.getPosition().y > activity.mapHeight )
            return true;

        // Hits the block of the map
        for( int i = 0; i < 10; i++ ) {
            // Hits horizontal wall
            if( (bullet.getPosition().x > game.map.wallHXPos[i] && bullet.getPosition().x < game.map.wallHXPos[i] + game.map.wallHArray[1].getWidth() ) &&
                    (bullet.getPosition().y > game.map.wallHYPos[i] && bullet.getPosition().y < game.map.wallHYPos[i] + game.map.wallHArray[1].getHeight() ) ) {
                return true;
            }

            // Hits vertical wall
            if( (bullet.getPosition().x > game.map.wallVXPos[i] && bullet.getPosition().x < game.map.wallVXPos[i] + game.map.wallVArray[1].getWidth() ) &&
                    (bullet.getPosition().y > game.map.wallVYPos[i] && bullet.getPosition().y < game.map.wallVYPos[i] + game.map.wallVArray[1].getHeight() ) ) {
                return true;
            }
        }

        return false;
    }

    public void setBitmapColor() {
        Log.i("Function: SetColor",""+bullet.getColor());
        switch(bullet.getColor()) {
            case RED:
                bulletMap = activity.normBitmap(activity.loadBitmap(R.drawable.bullet1p));
                break;
            case GREEN:
                bulletMap = activity.normBitmap(activity.loadBitmap(R.drawable.bullet2p));
                break;
            case BLUE:
                bulletMap = activity.normBitmap(activity.loadBitmap(R.drawable.bullet3p));
                break;
            case YELLOW:
                bulletMap = activity.normBitmap(activity.loadBitmap(R.drawable.bullet4p));
                break;
        }
    }
}
