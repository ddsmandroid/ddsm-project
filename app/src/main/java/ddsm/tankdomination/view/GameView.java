package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.GameStateType;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.PlayerColor;
import ddsm.tankdomination.model.Position;

import static ddsm.tankdomination.model.PlayerColor.BLUE;
import static ddsm.tankdomination.model.PlayerColor.RED;
import static ddsm.tankdomination.model.PlayerColor.YELLOW;

/**
 *
 *
 */
public class GameView extends GameState {
    Bitmap explosion;
    boolean showOverlay;
    long timeElapsed = 0;
    long timeToPlay = 2500;
    int deathCount =0;

    public GameView(GameActivity a ) {
        activity = a;
        gameStateType = GameStateType.TUTORIAL;

        controller = new Controller(this);
        map = new Map(this);
        showOverlay = true;
        playing = true;

        explosion = activity.loadBitmap(R.drawable.explosion);

        if( activity.musicOn ) {
            mediaPlayer = MediaPlayer.create(activity.getContext(), R.raw.battle_music);
            mediaPlayer.start();
            mediaPlayer.setLooping(true);
        }

        players = new TreeMap<String,PlayerObject>();
        for(Player player: activity.model.getPlayers().values()){
            if(!players.containsKey(player.getUserId())){
                if(player.getUserId()==activity.model.getUserId()){
                    PlayerObject playerObj = new PlayerObject(this,player);
                    players.put(player.getUserId(), playerObj);
                }
                else{
                    PlayerObject playerObj = new BluetoothPlayerObject(this,player);
                    players.put(player.getUserId(), playerObj);
                }
            }
        }
    }


    public void update(long deltaTime) {
        activity.network.update();
        // Update playerObject
        for(PlayerObject player: players.values()){
            player.update(deltaTime);
        }

        deathCount = 0;

        for(PlayerObject player: players.values()){
            if(player.player.getUserId()==activity.model.getUserId()){
                playing = player.player.getTank().getAlive();
            }
            else{
                if(!player.player.getTank().getAlive()){
                    deathCount++;
                }
            }
        }

        if(deathCount == activity.model.getNumPlayers()-1) {playing = false;}

        activity.network.sendTank(players.get(activity.model.getUserId()).player.getTank());
    }

    public void draw() {
        // Display countdown for new round


        if( activity.holder.getSurface().isValid() ) {
            // Lock the canvas to draw
            activity.canvas = activity.holder.lockCanvas();

            //@TODO: Draw map instead of solid colour
            // BACK GROUND COLOR
            activity.canvas.drawColor(Color.argb(255, 0, 0, 0));

            map.draw();

            if (activity.debug) {
                // Choose brush color for Drawing
                activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                activity.paint.setTextSize(activity.norm(60.f));

                // Display current FPS on screen
                activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
            }

            for(PlayerObject player: players.values()){
                player.draw();
                if(!player.player.getTank().getAlive()){
                    activity.canvas.drawBitmap(explosion, player.tankObj.matrix, null);
                }
            }

            controller.draw();

            // Draw everything to the screen and unlock surface
            activity.holder.unlockCanvasAndPost(activity.canvas);
        }
    }

    public boolean touchEvent(MotionEvent event) {
        int pointerIndex = MotionEventCompat.getActionIndex(event);
        float x = MotionEventCompat.getX(event, pointerIndex);
        float y = MotionEventCompat.getY(event, pointerIndex);
        if (!players.get(activity.model.getUserId()).player.getTank().getAlive() || deathCount == activity.model.getNumPlayers()-1){
            int action = MotionEventCompat.getActionMasked(event);
            this.mediaPlayer.stop();
            if( action == MotionEvent.ACTION_DOWN )
                activity.setGameState( GameStateType.MENU );
        }

        return controller.touchEvent(event);
    }

    @Override
    public void sendBullet(Bullet bullet) {
        activity.network.sendBullet(bullet);
    }

    @Override
    public void sendHit(String type, String id, String ownerId) {
        activity.network.sendHit(type,id,ownerId);
    }

    public LinkedList<PlayerObject> getPlayersList() {
        LinkedList<PlayerObject> pList = new LinkedList<PlayerObject>();
        // Add players to linkedList
        for (TreeMap.Entry<String,PlayerObject> entry : players.entrySet()) {
            PlayerObject p = entry.getValue();
            pList.add(p);
        }
        return pList;
    }


}
