package ddsm.tankdomination.view;

import java.util.TreeMap;

import ddsm.tankdomination.GameState;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.PlayerColor;

public class BotObject extends PlayerObject {
    PlayerObject target;
    long timeAlive;
    int spawnID;

    public BotObject(GameState gs, Player plr, PlayerObject t) {
        super(gs, plr);
        player.setUserId("BOT_" + player.getUserId());
        id = player.getUserId();


        target = t;
        timeAlive = 0;
    }

    public void draw() { super.draw(); }

    public void update(long dt) {
        super.update(dt);
        timeAlive += dt;
    }

    public PlayerObject bulletDetection() {
        for (TreeMap.Entry<String,PlayerObject> entry : gameState.players.entrySet()) {
            if (entry.getKey() == id) continue; // Skip own bullet detection
            if (entry.getKey().regionMatches(false, 0, "BOT_", 0, 4)) continue; // Skip other bots
            PlayerObject p = entry.getValue();

            // Check playerObject p's bullets
            for( BulletObject aBullet : p.bullets.values() ) {
                if( this.player.getTank().getShieldLeft() > 0 ) {

                    if( (aBullet.bullet.getPosition().x > tankObj.tank.getPosition().x - tankObj.tankTurret.getWidth()  && aBullet.bullet.getPosition().x < tankObj.tank.getPosition().x + tankObj.tankTurret.getWidth()) &&
                            (aBullet.bullet.getPosition().y > tankObj.tank.getPosition().y - tankObj.tankTurret.getHeight()  && aBullet.bullet.getPosition().y < tankObj.tank.getPosition().y + tankObj.tankTurret.getHeight())) {
                        p.removeBullet(aBullet);
                        return p;
                    }
                }
                else {
                    if( (aBullet.bullet.getPosition().x > tankObj.tank.getPosition().x - tankObj.tankTurret.getWidth()/2  && aBullet.bullet.getPosition().x < tankObj.tank.getPosition().x + tankObj.tankTurret.getWidth()/2.f) &&
                            (aBullet.bullet.getPosition().y > tankObj.tank.getPosition().y - tankObj.tankTurret.getHeight()/2  && aBullet.bullet.getPosition().y < tankObj.tank.getPosition().y + tankObj.tankTurret.getHeight()/2.f)) {
                        p.removeBullet(aBullet);

                        return p;
                    }
                }
            }
        }
        return null;
    }

    public void getControls() {
        // Get Move State from Controller
        if (target.getPosition().x <= getPosition().x - tankObj.tankTurretScaled.getWidth())
            tankObj.tank.getMoveState().h = -0.5f;
        else if (target.getPosition().x > getPosition().x + tankObj.tankTurretScaled.getWidth())
            tankObj.tank.getMoveState().h = 0.5f;

        if (target.getPosition().y <= getPosition().y - tankObj.tankTurretScaled.getHeight())
            tankObj.tank.getMoveState().v = -0.5f;
        else if (target.getPosition().y > getPosition().y + tankObj.tankTurretScaled.getHeight())
            tankObj.tank.getMoveState().v = 0.5f;

        tankObj.tank.getMoveState().angle = (float) Math.toDegrees(Math.atan2(tankObj.tank.getMoveState().v, tankObj.tank.getMoveState().h));
        tankObj.tank.setTurretAngle((float) Math.toDegrees(Math.atan2(target.getPosition().y - getPosition().y, target.getPosition().x - getPosition().x)));
    }

    @Override
    public void getTurretFire(long dt) {
        // Check if turret if fired
        timeElapsed += dt;
        if( numBullet != maxBullet && timeElapsed  > waitMillis ) {
            timeElapsed = 0;
            addBullet(player.getTank().getPosition().x,
                    player.getTank().getPosition().y,
                    tankObj.tank.getTurretAngle());
        }
    }
}
