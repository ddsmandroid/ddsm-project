package ddsm.tankdomination.view;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.GameStateType;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;

/**
 *
 */


public class JoinGameView extends GameState {
    int hostSelected;
    float linePerHost = activity.norm(100.f); // Total offset between hosts on list
    float hostListOffset = activity.norm(300.f); // Height of each host entry
    float hostListTextSize = activity.norm(75.f); // Text size of hosts
    BluetoothDevice host;
    Button buttonJoinGame;
    Button status;
    LinkedList<BluetoothDevice> devices;


    public JoinGameView(GameActivity a) {
        activity = a;
        gameStateType = GameStateType.JOIN;
        hostSelected = -1;

        status = new Button(
                activity,
                "Discovering Games",
                activity.mapWidth/2.f,
                100.f,
                activity.mapWidth,
                100.f,
                60.f,
                Color.argb(64, 0, 0, 0),
                Color.argb(255, 255, 255, 255));
        buttonJoinGame = new Button(
                activity,
                "Join_Game",
                activity.mapWidth - 400.f,
                activity.mapHeight - 100.f,
                375.f,
                200.f,
                60.f,
                Color.argb(255, 120, 60, 0),
                Color.argb(255, 255, 255, 255));

        if( activity.musicOn ) {
            mediaPlayer = MediaPlayer.create(activity.getContext(), R.raw.bensound_epic);
            mediaPlayer.start();
            mediaPlayer.setLooping(true);
        }


        a.network.startAdapter();
        devices = new LinkedList<BluetoothDevice>();
        activity.network.discoverBondedDevices();
        // Add players
        players = new TreeMap<String,PlayerObject>();
        players.put(activity.model.getUserId(), new PlayerObject(this, activity.model.getPlayer(activity.model.getUserId())));
    }

    public void update(long dt) {
        devices = activity.network.getDevices();
        activity.network.updateState();
        for(Player player: activity.model.getPlayers().values()){
            if(!players.containsKey(player.getUserId())){
                players.put(player.getUserId(), new PlayerObject(this, player));
            }
        }
    }

    public void draw() {
        if( activity.holder.getSurface().isValid() ) {
            // Lock the canvas to draw
            activity.canvas = activity.holder.lockCanvas();

            //@TODO: Draw map instead of solid colour
            // BACK GROUND COLOR
            activity.canvas.drawColor( Color.argb( 255, 26, 128, 182) );

            // Choose brush color for Drawing
            activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
            activity.paint.setTextSize(activity.norm(60.f));

            if (activity.debug)
            {
                // Choose brush color for Drawing
                activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                activity.paint.setTextSize(activity.norm(60.f));
                activity.paint.setTextAlign(Paint.Align.CENTER);

                // Display current FPS on screen
                activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
            }

            // Draw buttons
            status.draw();
            buttonJoinGame.draw();
            drawHostList();

            // Draw everything to the screen and unlock surface
            activity.holder.unlockCanvasAndPost( activity.canvas );
        }
    }

    public boolean touchEvent(MotionEvent event) {
        int pointerIndex = MotionEventCompat.getActionIndex(event);
        float x = MotionEventCompat.getX(event, pointerIndex);
        float y = MotionEventCompat.getY(event, pointerIndex);

        int action = MotionEventCompat.getActionMasked( event );

        if( action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN ) {
            // Check if buttons are pressed
            if (buttonJoinGame.isPressed(x, y)) {
                if (hostSelected > -1) {
                    activity.network.connectUser(host.getAddress());
                    status.setText("Waiting For Host to Start Game");
                } else {
                    status.setText("Select a Game");
                }
                return true;
            }
            else {
                // Check if a host is selected
                hostIsPressed(x, y);
            }

        } else if( action == MotionEvent.ACTION_CANCEL ) {

        }

        return true;
    }

    @Override
    public void sendBullet(Bullet bullet) {
        // do nothing
    }

    @Override
    public void sendHit(String type, String id, String ownerId) {

    }

    private void drawHostList() {
        float pos_y = hostListOffset + hostListTextSize;

        activity.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        activity.paint.setTextAlign(Paint.Align.LEFT);
        activity.paint.setTextSize(hostListTextSize);

        int i = 0;
        if(devices != null){
            if(!devices.isEmpty()){
                for (BluetoothDevice device : devices) {
                    if (i == hostSelected) activity.paint.setColor(Color.argb(255, 255, 0, 0));
                    else activity.paint.setColor(Color.argb(255, 255, 255, 255));
                    if(device!=null){
                        String name = device.getName();
                        if(name == "" || name == null) name = device.getAddress();
                        activity.canvas.drawText(
                                name,
                                10.f,
                                pos_y,
                                activity.paint);
                        pos_y += linePerHost;
                        i++;
                    }
                }
            }
        }

    }

    private boolean hostIsPressed(float x, float y) {
        // Convert y to line
        if(devices != null) {
            if (!devices.isEmpty()) {
                int entry = (int) Math.floor((y - hostListOffset) / linePerHost);
                if (entry >= 0 && entry < devices.size()) {
                    hostSelected = entry;
                    host = devices.get(entry);
                    buttonJoinGame.setBackground(Color.argb(255, 249, 129, 0));
                    status.setText("Connecting to " + host.getName());
                    return true;
                }
            }
        }
        return false;
    }

    public LinkedList<PlayerObject> getPlayersList() {
        LinkedList<PlayerObject> pList = new LinkedList<PlayerObject>();
        // Add players to linkedList
        for (TreeMap.Entry<String,PlayerObject> entry : players.entrySet()) {
            PlayerObject p = entry.getValue();
            pList.add(p);
        }
        return pList;
    }
}
