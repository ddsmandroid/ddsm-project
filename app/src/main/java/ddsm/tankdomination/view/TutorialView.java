package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.GameStateType;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.Position;

import static ddsm.tankdomination.model.PlayerColor.BLUE;
import static ddsm.tankdomination.model.PlayerColor.RED;
import static ddsm.tankdomination.model.PlayerColor.YELLOW;

/**
 *
 *
 */
public class TutorialView extends GameState {
    PlayerObject playerObject;
    BotObject comp1, comp2;
    Bitmap explosion;
    boolean showOverlay;
    boolean leftTouch = false;
    boolean rightTouch = false;
    boolean isReady = false;
    long timeElapsed = 0;
    long timeToPlay = 2500;

    public TutorialView(GameActivity a ) {
        activity = a;
        gameStateType = GameStateType.TUTORIAL;

        // Add players
        players = new TreeMap<String,PlayerObject>();
        Player player1 = new Player("0000","0000", YELLOW);
        playerObject = new PlayerObject(this, player1);
        players.put(playerObject.id, playerObject);
        // Bots
        Player compPlayer1 = new Player("bbb0", "bbb0", RED);
        comp1 = new BotObject(this, compPlayer1, playerObject);
        players.put(comp1.id, comp1);
        comp1.player.getTank().setPosition(new Position(activity.mapWidth - 100 , 50));

        Player compPlayer2 = new Player("bbb1","bbb1", BLUE);
        comp2 = new BotObject(this, compPlayer2, playerObject);
        players.put(comp2.id, comp2);
        comp2.player.getTank().setPosition(new Position(activity.mapWidth - 100 , activity.mapHeight - 50));

        controller = new Controller(this);
        map = new Map(this);
        showOverlay = true;
        playing = true;

        explosion = activity.normBitmap(activity.loadBitmap(R.drawable.explosion));

        mediaPlayer = MediaPlayer.create(activity.getContext(), R.raw.bensound_epic);
        if( activity.musicOn ) {
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }
    }


    public void update(long deltaTime) {
        // Update playerObject
        if( (!leftTouch || !rightTouch)  && !isReady) {
            playerObject.update(deltaTime);
        }else if( leftTouch && rightTouch && !isReady ) {
            playerObject.update(deltaTime);
            timeElapsed += deltaTime;
            if( timeElapsed > timeToPlay ) {
                isReady = true;
            }
        }

        if (showOverlay) return;
        if (!playing) return;

        playerObject.update(deltaTime);
        // Update comp
        comp1.update(deltaTime);
        comp2.update(deltaTime);

        int deathCount = 0;
        playing = playerObject.player.getTank().getAlive();
        for (TreeMap.Entry<String,PlayerObject> entry : players.entrySet()) {
            if (!entry.getValue().player.getTank().getAlive()) deathCount++;
        }
        if(deathCount == 2) {playing = false;}
    }

    public void draw() {
        if( activity.holder.getSurface().isValid() ) {
            // Lock the canvas to draw
            activity.canvas = activity.holder.lockCanvas();

            //@TODO: Draw map instead of solid colour
            // BACK GROUND COLOR
            activity.canvas.drawColor(Color.argb(255, 0, 0, 0));

            map.draw();

            if (activity.debug)
            {
                // Choose brush color for Drawing
                activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                activity.paint.setTextSize(activity.norm(60.f));

                // Display current FPS on screen
                activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
            }

            playerObject.draw();
            if (!playerObject.player.getTank().getAlive()) activity.canvas.drawBitmap(explosion, playerObject.tankObj.matrix, null);

            if( isReady ) {
                comp1.draw();
                if (!comp1.player.getTank().getAlive()) activity.canvas.drawBitmap(explosion, comp1.tankObj.matrix, null);
                comp2.draw();
                if (!comp2.player.getTank().getAlive())
                    activity.canvas.drawBitmap(explosion, comp2.tankObj.matrix, null);
            }

            if (showOverlay)
            {
                // Draw borders
                if( !leftTouch ) {
                    // Draw alpha areas for left and right thumbsticks
                    activity.paint.setStyle(Paint.Style.FILL);
                    activity.paint.setColor(Color.argb(100, 0, 0, 0));
                    activity.canvas.drawRect(activity.normX(0.f), activity.normY(0.f), activity.normX((float)activity.mapWidth/2), activity.normY((float)activity.mapHeight), activity.paint);
                    // Left
                    activity.paint.setStyle(Paint.Style.STROKE);
                    activity.paint.setColor(Color.argb(255, 0, 0, 0));
                    activity.canvas.drawRect(activity.normX(0.f), activity.normY(0.f), activity.normX((float) activity.mapWidth / 2.f), activity.normY((float) activity.mapHeight), activity.paint);
                    activity.paint.setTextSize(activity.norm(80.f));
                    activity.drawText("Press here to move your tank", activity.normX(activity.mapWidth / 4.f), activity.normY(activity.mapHeight / 2.f), activity.paint);
                } else {
                    playerObject.draw();
                    controller.draw();
                }

                if( !rightTouch ) {
                    // Draw alpha areas for left and right thumbsticks
                    activity.paint.setStyle(Paint.Style.FILL);
                    activity.paint.setColor(Color.argb(100, 0, 0, 0));
                    activity.canvas.drawRect(activity.normX((float)activity.mapWidth/2), activity.normY(0.f), activity.normX((float)activity.mapWidth), activity.normY((float)activity.mapHeight), activity.paint);
                    // Right
                    activity.paint.setStyle(Paint.Style.STROKE);
                    activity.paint.setColor(Color.argb(255, 0, 0, 0));
                    activity.canvas.drawRect(activity.normX((float) activity.mapWidth / 2.f), activity.normY((float) activity.mapHeight), activity.normX((float) activity.mapWidth), activity.normY((float) activity.mapHeight), activity.paint);
                    activity.paint.setTextSize(activity.norm(80.f));
                    activity.drawText("Press here to shoot your turret", activity.normX(3.f * activity.mapWidth / 4.f), activity.normY(activity.mapHeight / 2.f), activity.paint);
                } else {
                    playerObject.draw();
                    controller.draw();
                }

                if( isReady ) {
                    activity.drawText("You now know how to play! Your shield will last you 3 bullets.", activity.normX(activity.mapWidth / 2.f), activity.normY(activity.mapHeight/2 - 100.f), activity.paint);
                    activity.drawText("Press anywhere to play against some bots", activity.normX(activity.mapWidth / 2.f), activity.normY(activity.mapHeight/2), activity.paint);
                }

            } else if( playing ) {

                // Draw controller overlay
                controller.draw();

            } else if( !playerObject.player.getTank().getAlive()) {
                activity.paint.setColor(Color.WHITE);
                activity.paint.setTextSize(activity.norm(100.f));
                activity.drawText("You lost. Thank you for trying the tutorial.", activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f), activity.paint);
                activity.paint.setTextSize(activity.norm(80.f));
                activity.drawText("Click to return to main menu.", activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f+100), activity.paint);
            } else if(!comp1.player.getTank().getAlive() && !comp2.player.getTank().getAlive()) {
                activity.paint.setTextSize(activity.norm(100.f));
                activity.drawText("You won. Thank you for trying the tutorial.", activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f), activity.paint);
                activity.paint.setTextSize(activity.norm(80.f));
                activity.drawText("Click to return to main menu.", activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f+100), activity.paint);
            }

            // Draw everything to the screen and unlock surface
            activity.holder.unlockCanvasAndPost(activity.canvas);
        }
    }

    public boolean touchEvent(MotionEvent event) {
        int pointerIndex = MotionEventCompat.getActionIndex(event);
        float x = MotionEventCompat.getX(event, pointerIndex);
        float y = MotionEventCompat.getY(event, pointerIndex);
        if (showOverlay)
        {
            int action = MotionEventCompat.getActionMasked(event);
            if( action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
                if( x < activity.mapWidth/2 )
                    leftTouch = true;
                else if( x > activity.mapWidth/2 )
                    rightTouch = true;

                if( isReady )
                    showOverlay = false;
            }
        } else if( !playerObject.player.getTank().getAlive() || (!comp1.player.getTank().getAlive() && !comp2.player.getTank().getAlive())) {
            int action = MotionEventCompat.getActionMasked(event);
            this.mediaPlayer.stop();
            if( action == MotionEvent.ACTION_DOWN )
                activity.setGameState( GameStateType.MENU );
        }

        return controller.touchEvent(event);
    }

    public LinkedList<PlayerObject> getPlayersList() {
        LinkedList<PlayerObject> pList = new LinkedList<PlayerObject>();
        // Add players to linkedList
        for (TreeMap.Entry<String,PlayerObject> entry : players.entrySet()) {
            PlayerObject p = entry.getValue();
            pList.add(p);
        }
        return pList;
    }

    @Override
    public void sendBullet(Bullet bullet) {
        // do nothing
    }

    @Override
    public void sendHit(String type, String id, String ownerId) {

    }
}
