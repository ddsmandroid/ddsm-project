package ddsm.tankdomination.view;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Position;

import static ddsm.tankdomination.GameStateType.MENU;

/**
 *
 */

public class PauseView {
    GameState gameState;
    GameActivity activity;
    Button buttonMenu, buttonPlay;
    ButtonBitmap buttonMusic;
    ButtonBitmap buttonSoundEffects;
    float borderOff = 100.f;

    public PauseView(GameState gs) {
        gameState = gs;
        activity = gs.activity;

        // Button
        buttonPlay = new Button(
                activity,
                "Play",
                activity.mapWidth/2.f,
                activity.mapHeight/2.f,
                800.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));

        buttonMenu = new Button(
                activity,
                "Return To Menu",
                activity.mapWidth/2.f,
                activity.mapHeight/2.f + 250.f,
                800.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));

        // Bitmap buttons
        buttonMusic = new ButtonBitmap(
                activity,
                activity.loadBitmap(R.drawable.music_on),
                activity.loadBitmap(R.drawable.music_off),
                activity.mapWidth/2.f - 100.f,
                activity.mapHeight - 2.f * borderOff
        );

        buttonSoundEffects = new ButtonBitmap(
                activity,
                activity.loadBitmap(R.drawable.effect_on),
                activity.loadBitmap(R.drawable.effect_off),
                activity.mapWidth/2.f + 100.f,
                activity.mapHeight - 2.f * borderOff
        );
    }

    public void draw() {
        Rect rect;

        // Draw all player names
        int i = 0;
        LinkedList<PlayerObject> players = gameState.getPlayersList();
        activity.paint.setTextSize(activity.norm(60.f));
        for (PlayerObject player : players) {
            // If player is below half print name above
            Position pos = player.getPosition();
            if (pos.y > activity.mapHeight / 2.f) {
                activity.drawText(player.player.getName(), activity.normX(pos.x), activity.normY(pos.y - 2.f * player.tankObj.tankBaseScaled.getHeight()), activity.paint);
            } else {
                activity.drawText(player.player.getName(), activity.normX(pos.x), activity.normY(pos.y + 2.f * player.tankObj.tankBaseScaled.getHeight() + 60.f), activity.paint);
            }
        }

        // Draw rectangle to indicate game is paused
        activity.paint.setColor(Color.argb(100, 0, 0, 0));
        activity.paint.setStyle(Paint.Style.FILL);
        rect = new Rect((int)activity.normX(borderOff), (int)activity.normY(borderOff), (int)activity.normX((float)activity.mapWidth - borderOff), (int)activity.normY((float)activity.mapHeight - borderOff));
        activity.canvas.drawRect(rect, activity.paint);
        activity.paint.setColor(Color.argb(180, 0, 0, 0));
        activity.paint.setStyle(Paint.Style.STROKE);
        activity.canvas.drawRect(rect, activity.paint);

        activity.paint.setTextSize(activity.norm(100.f));
        activity.drawText("Paused", activity.normX(activity.mapWidth/2.f), activity.normY(borderOff + 120.f), activity.paint);

        buttonPlay.draw();
        buttonMenu.draw();

        // Draw background for sound buttons
        activity.paint.setColor(Color.argb(100, 255, 255, 255));
        activity.paint.setStyle(Paint.Style.FILL);
        rect = new Rect((int)activity.normX(activity.mapWidth/2.f - 200.f), (int)activity.normY(activity.mapHeight - 2.5f * borderOff), (int)activity.normX(activity.mapWidth/2.f + 200.f), (int)activity.normY((float)activity.mapHeight - 1.5f * borderOff));
        activity.canvas.drawRect(rect, activity.paint);
        activity.paint.setColor(Color.argb(180, 0, 0, 0));
        activity.paint.setStyle(Paint.Style.STROKE);
        activity.canvas.drawRect(rect, activity.paint);
        buttonMusic.draw(activity.musicOn);
        buttonSoundEffects.draw(activity.effectsOn);
    }

    public void touchEvent(float x, float y) {
        // Check if sound is pressed
        if(buttonMusic.isPressed(x, y)) {
            activity.musicOn = !activity.musicOn;
            if( !activity.musicOn && gameState.mediaPlayer.isPlaying() )
                gameState.mediaPlayer.pause();
            else
                gameState.mediaPlayer.start();
        } else if(buttonSoundEffects.isPressed(x, y))
            activity.effectsOn = !activity.effectsOn;
        else if(buttonPlay.isPressed(x, y)){
            activity.togglePause();
        }
        else if(buttonMenu.isPressed(x, y)){
            activity.setGameState(MENU);
        }
    }
}
