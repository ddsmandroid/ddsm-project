package ddsm.tankdomination.view;

import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.GameStateType;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;

/**
 *
 */

public class CreateGameView extends GameState {
    Button buttonStartGame;
    Button status;

    public CreateGameView(GameActivity a) {
        activity = a;
        gameStateType = GameStateType.CREATE;

        status = new Button(
                activity,
                "Looking For Players",
                activity.mapWidth/2.f,
                100.f,
                activity.mapWidth,
                100.f,
                60.f,
                Color.argb(64, 0, 0, 0),
                Color.argb(255, 255, 255, 255));
        buttonStartGame = new Button(
                activity,
                "Start_Game",
                activity.mapWidth - 400.f,
                activity.mapHeight - 100.f,
                375.f,
                200.f,
                60.f,
                Color.argb(255, 249, 129, 0),
                Color.argb(255, 255, 255, 255));

        if( activity.musicOn ) {
            mediaPlayer = MediaPlayer.create(activity.getContext(), R.raw.bensound_epic);
            mediaPlayer.start();
            mediaPlayer.setLooping(true);
        }


        // Add players

        players = new TreeMap<String,PlayerObject>();
        players.put(activity.model.getUserId(), new PlayerObject(this, activity.model.getPlayer(activity.model.getUserId())));
    }

    public void update(long dt) {
        activity.network.updateState();
        for(Player player: activity.model.getPlayers().values()){
            if(!players.containsKey(player.getUserId())){
                players.put(player.getUserId(), new PlayerObject(this, player));
            }
        }
    }

    public void draw() {
        if( activity.holder.getSurface().isValid() ) {
            // Lock the canvas to draw
            activity.canvas = activity.holder.lockCanvas();

            //@TODO: Draw map instead of solid colour
            // BACK GROUND COLOR
            activity.canvas.drawColor( Color.argb( 255, 26, 128, 182) );

            // Choose brush color for Drawing
            activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
            activity.paint.setTextSize(activity.norm(60.f));

            if (activity.debug)
            {
                // Choose brush color for Drawing
                activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                activity.paint.setTextSize(activity.norm(60.f));
                activity.paint.setTextAlign(Paint.Align.CENTER);

                // Display current FPS on screen
                activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
            }

            // Draw buttons
            status.draw();
            buttonStartGame.draw();

            // Draw everything to the screen and unlock surface
            activity.holder.unlockCanvasAndPost( activity.canvas );
        }
    }

    public boolean touchEvent(MotionEvent event) {
        int pointerIndex = MotionEventCompat.getActionIndex(event);
        float x = MotionEventCompat.getX(event, pointerIndex);
        float y = MotionEventCompat.getY(event, pointerIndex);

        int action = MotionEventCompat.getActionMasked( event );

        if( action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN ) {
            // Check if buttons are pressed
            if (buttonStartGame.isPressed(x, y)) {
                activity.network.startServer();
                return true;
            }

        } else if( action == MotionEvent.ACTION_CANCEL ) {

        }

        return true;
    }

    @Override
    public void sendBullet(Bullet bullet) {
        // do nothing
    }

    @Override
    public void sendHit(String type, String id, String ownerId) {
        // do nothing
    }

    public LinkedList<PlayerObject> getPlayersList() {
        LinkedList<PlayerObject> pList = new LinkedList<PlayerObject>();
        // Add players to linkedList
        for (TreeMap.Entry<String,PlayerObject> entry : players.entrySet()) {
            PlayerObject p = entry.getValue();
            pList.add(p);
        }
        return pList;
    }
}
