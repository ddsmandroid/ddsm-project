package ddsm.tankdomination.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.R;


/**
 * Created by david on 2016-11-02.
 */

public class Map {
    Bitmap              wall1, wall2, wall3, wall4, wall5, wall6, wall7, wall8, wall9, wall10;
    Bitmap              wall11, wall12, wall13, wall14, wall15, wall16, wall17, wall18, wall19, wall20;
    Bitmap[]            wallHArray;
    Bitmap[]            wallVArray;
    Bitmap[]            wallHArrayScaled;
    Bitmap[]            wallVArrayScaled;

    GameActivity activity;

    float[] wallHXPos 	= {210, 210, 350, 350, 1050, 1050, 1680, 1680, 1900, 1900};
    float[]	wallHYPos 	= {130, 1270, 300, 1100, 200, 1200, 300, 1100, 130, 1270};
    float[] wallVXPos 	= {171, 171, 400, 171, 1050, 171, 1460, 2160, 2349, 2349};
    float[]	wallVYPos 	= {130, 910, 520, 910, 520, 910, 520, 520, 130, 910};

    float[] wallHXPosScaled;
    float[] wallHYPosScaled;
    float[] wallVXPosScaled;
    float[] wallVYPosScaled;

    public Map(GameState gs) {
        activity = gs.activity;
        wall1			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall2			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall3			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall4			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall5			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall6			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall7			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall8			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall9			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall10			= activity.loadBitmap(R.drawable.wall_horizontal );
        wall11			= activity.loadBitmap(R.drawable.wall_vertical );
        wall12			= activity.loadBitmap(R.drawable.wall_vertical );
        wall13			= activity.loadBitmap(R.drawable.wall_vertical );
        wall14			= activity.loadBitmap(R.drawable.wall_vertical );
        wall15			= activity.loadBitmap(R.drawable.wall_vertical );
        wall16			= activity.loadBitmap(R.drawable.wall_vertical );
        wall17			= activity.loadBitmap(R.drawable.wall_vertical );
        wall18			= activity.loadBitmap(R.drawable.wall_vertical );
        wall19			= activity.loadBitmap(R.drawable.wall_vertical );
        wall20			= activity.loadBitmap(R.drawable.wall_vertical );
        wallHArray      = new Bitmap[] {wall1, wall2, wall3, wall4, wall5, wall6, wall7, wall8, wall9, wall10};
        wallVArray      = new Bitmap[] {wall11, wall12, wall13, wall14, wall15, wall16, wall17, wall18, wall19, wall20};

        // Set size of scaled arrays
        wallHArrayScaled = new Bitmap[wallHArray.length];
        wallHXPosScaled = new float[wallHXPos.length];
        wallHYPosScaled = new float[wallHYPos.length];
        wallVArrayScaled = new Bitmap[wallVArray.length];
        wallVXPosScaled = new float[wallVXPos.length];
        wallVYPosScaled = new float[wallVYPos.length];

        // Load scaled bitmaps
        // Calculate the scaled positions of the walls
        for(int i = 0; i < wallHArray.length; i++) {
            wallHArrayScaled[i] = activity.normBitmap(wallHArray[i]);
            wallHXPosScaled[i] = activity.normX(wallHXPos[i]);
            wallHYPosScaled[i] = activity.normY(wallHYPos[i]);
        }
        for(int i = 0; i < wallVArray.length; i++) {
            wallVArrayScaled[i] = activity.normBitmap(wallVArray[i]);
            wallVXPosScaled[i] = activity.normX(wallVXPos[i]);
            wallVYPosScaled[i] = activity.normY(wallVYPos[i]);
        }
    }

    public void draw() {
        // Draw Map background
        activity.paint.setColor( Color.argb( 255, 26, 128, 182) );
        activity.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        activity.canvas.drawRect(activity.normX(0.f), activity.normY(0.f), activity.normX(activity.mapWidth), activity.normY(activity.mapHeight), activity.paint);

        for(int i=0; i < wallHArray.length; i++){
            activity.canvas.drawBitmap( wallHArrayScaled[i], wallHXPosScaled[i], wallHYPosScaled[i], null );
            activity.canvas.drawBitmap( wallVArrayScaled[i], wallVXPosScaled[i], wallVYPosScaled[i], null );
        }
    }




}
