package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import ddsm.tankdomination.GameState;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.*;

public class Controller {
    GameState gameState;

    Bitmap innerJoystick;
    Bitmap outerJoystick;

    public MoveState leftMoveState;
    public MoveState rightMoveState;
    public Position leftBase;
    public Position rightBase;
    public int leftID;
    public int rightID;
    public boolean leftPress;
    public boolean rightPress;

    public Controller(GameState g) {
        gameState = g;

        innerJoystick   = BitmapFactory.decodeResource( gameState.activity.getResources(), R.drawable.image_button );
        outerJoystick = BitmapFactory.decodeResource( gameState.activity.getResources(), R.drawable.image_button_bg );

        leftPress = false;
        rightPress = false;

        leftMoveState = new MoveState();
        rightMoveState = new MoveState();
        leftBase = new Position();
        rightBase = new Position();
    }

    public void draw()
    {
        if( leftPress ) {
            // Draw base point of left joystick
            gameState.activity.canvas.drawBitmap(outerJoystick, leftBase.x - outerJoystick.getWidth()/2,
                    leftBase.y - outerJoystick.getHeight()/2, gameState.activity.paint );

            // Draw innerJoystick based on Move State
            gameState.activity.canvas.drawBitmap( innerJoystick,
                    ( leftBase.x - innerJoystick.getWidth() / 2 ) + leftMoveState.h * outerJoystick.getWidth() / 2,
                    ( leftBase.y - innerJoystick.getHeight() / 2 ) + leftMoveState.v * outerJoystick.getHeight() / 2, gameState.activity.paint );
        }

        if( rightPress ) {
            // Draw base point of right joystick
            gameState.activity.canvas.drawBitmap(outerJoystick, rightBase.x - outerJoystick.getWidth()/2,
                    rightBase.y - outerJoystick.getHeight()/2, gameState.activity.paint );

            // Draw innerJoystick based on Move State
            gameState.activity.canvas.drawBitmap( innerJoystick,
                    ( rightBase.x - innerJoystick.getWidth() / 2 ) + rightMoveState.h * outerJoystick.getWidth() / 2,
                    ( rightBase.y - innerJoystick.getHeight() / 2 ) + rightMoveState.v * outerJoystick.getHeight() / 2, gameState.activity.paint );
        }
    }

    public boolean touchEvent(MotionEvent event) {
        int pointerIndex = MotionEventCompat.getActionIndex(event);
        float x = MotionEventCompat.getX(event, pointerIndex);
        float y = MotionEventCompat.getY(event, pointerIndex);

        int action = MotionEventCompat.getActionMasked( event );

        if( action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN ) {
            // Left side of screen
            if (x < gameState.activity.getWidth() / 2) {
                if (!leftPress) {
                    leftMoveState.angle = (float) Math.toDegrees(Math.atan2( y - leftBase.y, x - leftBase.x));

                    // Set base position
                    leftBase.x = x;
                    leftBase.y = y;

                    // Save the ID of this pointer ( for dragging )
                    leftID = MotionEventCompat.getPointerId(event, pointerIndex);
                    leftPress = true;
                }
            } else {
                // Right side of screen
                if (!rightPress) {
                    // Calculate the angle
                    rightMoveState.angle = (float) Math.toDegrees(Math.atan2( y - rightBase.y, x - rightBase.x));

                    // Set base position
                    rightBase.x = x;
                    rightBase.y = y;

                    // Save the ID of this pointer ( for dragging )
                    rightID = MotionEventCompat.getPointerId(event, pointerIndex);
                    rightPress = true;
                }
            }
        } else if( action == MotionEvent.ACTION_MOVE ) {
            if (leftPress) {
                int leftIndex = MotionEventCompat.findPointerIndex(event, leftID);
                float leftX = MotionEventCompat.getX(event, leftIndex);
                float leftY = MotionEventCompat.getY(event, leftIndex);

                leftMoveState.angle = (float) Math.toDegrees(Math.atan2( leftY - leftBase.y, leftX - leftBase.x));

                // Calculate the distance as percentage between -1 and 1
                leftMoveState.h = (leftX - leftBase.x) / (outerJoystick.getWidth() / 2);
                if (leftMoveState.h > 1.f) leftMoveState.h = 1.f;
                if (leftMoveState.h < -1.f) leftMoveState.h = -1.f;
                leftMoveState.v = (leftY - leftBase.y) / (outerJoystick.getHeight() / 2);
                if (leftMoveState.v > 1.f) leftMoveState.v = 1.f;
                if (leftMoveState.v < -1.f) leftMoveState.v = -1.f;
            }

            if (rightPress) {
                int rightIndex = MotionEventCompat.findPointerIndex(event, rightID);
                float rightX = MotionEventCompat.getX(event, rightIndex);
                float rightY = MotionEventCompat.getY(event, rightIndex);

                // Calculate the angle
                rightMoveState.angle = (float) Math.toDegrees(Math.atan2( rightY - rightBase.y, rightX - rightBase.x));

                // Calculate the distance as percentage between -1 and 1
                rightMoveState.h = (rightX - rightBase.x) / (outerJoystick.getWidth() / 2);
                if (rightMoveState.h > 1.f) rightMoveState.h = 1.f;
                if (rightMoveState.h < -1.f) rightMoveState.h = -1.f;
                rightMoveState.v = (rightY - rightBase.y) / (outerJoystick.getHeight() / 2);
                if (rightMoveState.v > 1.f) rightMoveState.v = 1.f;
                if (rightMoveState.v < -1.f) rightMoveState.v = -1.f;
            }
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP ) {
            int index = MotionEventCompat.getActionIndex( event );
            int pointerId = MotionEventCompat.getPointerId( event, index );

            if( pointerId == leftID ) {
                leftPress = false;
                leftID = MotionEvent.INVALID_POINTER_ID;
                leftMoveState.h = 0.f;
                leftMoveState.v = 0.f;
            }

            if( pointerId == rightID ) {
                rightPress = false;
                rightID = MotionEvent.INVALID_POINTER_ID;
                rightMoveState.h = 0.f;
                rightMoveState.v = 0.f;
            }
        } else if( action == MotionEvent.ACTION_CANCEL ) {
            if (leftPress) {
                leftID = MotionEvent.INVALID_POINTER_ID;
                leftPress = false;
            }
            if (rightPress) {
                rightID = MotionEvent.INVALID_POINTER_ID;
                rightPress = false;
            }
        }

        return true;
    }
}
