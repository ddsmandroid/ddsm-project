package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.GameStateType;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.PlayerColor;
import ddsm.tankdomination.model.Position;

import static ddsm.tankdomination.GameStateType.CREATE;
import static ddsm.tankdomination.GameStateType.JOIN;
import static ddsm.tankdomination.GameStateType.SINGLE;
import static ddsm.tankdomination.GameStateType.TUTORIAL;

/**
 *
 */

public class Menu extends GameState {
    PlayerObject bot;
    Button buttonSPG;
    Button buttonTutorial;
    Button buttonStartGame;
    Button buttonJoinGame;
    Button buttonExit;
    ButtonBitmap buttonSoundEffects;
    ButtonBitmap buttonMusic;
    float titleSize;
    Path titlePath;
    String titleText;
    Position titlePos;

    public Menu(GameActivity a) {
        activity = a;
        gameStateType = GameStateType.MENU;

        // Setup title
        titleSize = activity.normX(150.f);
        titlePath = new Path();
        /*titlePath.addArc(
                new RectF(
                        activity.normX(100.f),
                        activity.normY(activity.mapHeight - 400.f),
                        activity.normY(activity.mapWidth - 100.f),
                        activity.normX(activity.mapHeight - 10.f)),
        -120.f, 135.f);*/
        titleText = "Tank Domination";
        titlePos = new Position(
                activity.norm(activity.mapWidth / 2.f),
                activity.norm(activity.mapHeight / 2.f - 450.f));

        buttonSPG = new Button(
                activity,
                "Single_Player Game",
                activity.mapWidth/2.f - 212.5f,
                activity.mapHeight/2.f - 250.f,
                375.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));
        buttonTutorial = new Button(
                activity,
                "Tutorial",
                activity.mapWidth/2.f + 212.5f,
                activity.mapHeight/2.f - 250.f,
                375.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));
        buttonStartGame = new Button(
                activity,
                "Create_Game",
                activity.mapWidth/2.f - 212.5f,
                activity.mapHeight/2.f,
                375.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));
        buttonJoinGame = new Button(
                activity,
                "Join_Game",
                activity.mapWidth/2.f + 212.5f,
                activity.mapHeight/2.f,
                375.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));
        buttonExit = new Button(
                activity,
                "Exit",
                activity.mapWidth/2.f,
                activity.mapHeight/2.f + 250.f,
                800.f,
                200.f,
                60.f,
                Color.argb(255, 15, 20, 41),
                Color.argb(255, 255, 255, 255));

        // Bitmap buttons
        buttonMusic = new ButtonBitmap(
                activity,
                activity.loadBitmap(R.drawable.music_on),
                activity.loadBitmap(R.drawable.music_off),
                activity.mapWidth - 100.f,
                activity.mapHeight - 150.f
        );

        buttonSoundEffects = new ButtonBitmap(
                activity,
                activity.loadBitmap(R.drawable.effect_on),
                activity.loadBitmap(R.drawable.effect_off),
                activity.mapWidth - 100.f,
                activity.mapHeight - 50.f
        );


        //Music: http://www.bensound.com
        mediaPlayer = MediaPlayer.create(activity.getContext(), R.raw.bensound_epic);
        if( activity.musicOn ) {
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }

        // Add players
        players = new TreeMap<String,PlayerObject>();
        bot = new PlayerObject(this, new Player("0","0", PlayerColor.RED));
        players.put("0", bot);
        bot.tankObj.tank.getPosition().x = 0.f;
        bot.tankObj.tank.getPosition().y = 1250.f;
    }

    public void update(long dt) {
        // Can place menu animations here if needed
        bot.tankObj.tank.getPosition().x += bot.tankObj.speed * (float)dt/1000.f;
        if (bot.tankObj.tank.getPosition().x > activity.mapWidth)
            bot.tankObj.tank.getPosition().x = 0.f;
    }

    public void draw() {
        if( activity.holder.getSurface().isValid() ) {
            // Lock the canvas to draw
            activity.canvas = activity.holder.lockCanvas();

            // BACK GROUND COLOR
            activity.canvas.drawColor( Color.argb( 255, 26, 128, 182) );

            if (activity.debug)
            {
                // Choose brush color for Drawing
                activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                activity.paint.setTextSize(activity.norm(60.f));
                activity.paint.setTextAlign(Paint.Align.CENTER);

                // Display current FPS on screen
                activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
            }

            // Draw Game title
            activity.paint.setColor(Color.WHITE);
            activity.paint.setStyle(Paint.Style.FILL);
            activity.paint.setTextSize(titleSize);
            activity.canvas.drawTextOnPath(titleText, titlePath, titlePos.x, titlePos.y, activity.paint);
            activity.paint.setColor(Color.BLACK);
            activity.paint.setStyle(Paint.Style.STROKE);
            activity.canvas.drawTextOnPath(titleText, titlePath, titlePos.x, titlePos.y, activity.paint);

            // Draw buttons
            buttonSPG.draw();
            buttonTutorial.draw();
            buttonStartGame.draw();
            buttonJoinGame.draw();
            buttonExit.draw();

            // Draw TankObject as animation
            bot.tankObj.draw();

            // Draw sound buttons
            buttonMusic.draw(activity.musicOn);
            buttonSoundEffects.draw(activity.effectsOn);

            // Draw everything to the screen and unlock surface
            activity.holder.unlockCanvasAndPost( activity.canvas );
        }
    }

    public boolean touchEvent(MotionEvent event) {
        int pointerIndex = MotionEventCompat.getActionIndex(event);
        float x = MotionEventCompat.getX(event, pointerIndex);
        float y = MotionEventCompat.getY(event, pointerIndex);

        int action = MotionEventCompat.getActionMasked( event );

        if( action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN ) {
            // Check if buttons are pressed
            if (buttonSPG.isPressed(x, y)) {
                activity.setGameState(SINGLE);
                return true;
            } else if (buttonTutorial.isPressed(x, y)) {
                activity.setGameState(TUTORIAL);
                return true;
            } else if (buttonStartGame.isPressed(x, y)) {
                activity.setGameState(CREATE);
                return true;
            } else if (buttonJoinGame.isPressed(x, y)) {
                activity.setGameState(JOIN);
                return true;
            } else if (buttonExit.isPressed(x, y)) {
                // Todo: Find more graceful way of exiting program
                activity.exitGame();
            }

            // Check if sound is pressed
            if(buttonMusic.isPressed(x, y)) {
                activity.musicOn = !activity.musicOn;
                if( !activity.musicOn && mediaPlayer.isPlaying() )
                    mediaPlayer.pause();
                else
                    mediaPlayer.start();
            } else if(buttonSoundEffects.isPressed(x, y))
                activity.effectsOn = !activity.effectsOn;

        } else if( action == MotionEvent.ACTION_CANCEL ) {

        }

        return true;
    }

    public LinkedList<PlayerObject> getPlayersList() {
        LinkedList<PlayerObject> pList = new LinkedList<PlayerObject>();
        return pList;
    }

    @Override
    public void sendBullet(Bullet bullet) {

    }

    @Override
    public void sendHit(String type, String id, String ownerId) {

    }
}
