package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Color;
import android.graphics.Rect;

import ddsm.tankdomination.GameState;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.*;

import static ddsm.tankdomination.GameState.activity;


/**
 * TankObject class for displaying and moving tanks. Will be used by local and remote players
 */

// Todo: change all positions to map positions as opposed to phone positions. Draw function should use norm functions to convert positions back to phone scale

public class TankObject {
    GameState gameState;
    PlayerObject playerObj;
    Bitmap tankBase;
    Bitmap tankTurret;
    Bitmap explosion;
    Bitmap tankBaseScaled;
    Bitmap tankTurretScaled;
    Tank tank;
    String id;
    Matrix matrix;
    int shieldHit; // Will keep track of when shield got hit


    // TankObject constants
    float speed = 120.f; // Should be map pixels per second
    // Is currently phone pixels per seconds

    public TankObject(GameState gs, PlayerObject po, Tank tk) {
        playerObj = po;
        gameState = gs;

        tankBase = activity.loadBitmap(R.drawable.tank_base);

        // Default tank color to yellow
        tankTurret = activity.loadBitmap(R.drawable.tank_turretp4);

        explosion = activity.loadBitmap(R.drawable.explosion);

        tankBaseScaled = activity.normBitmap(tankBase);
        tankTurretScaled = activity.normBitmap(tankTurret);

        //map = new Map(gameState);
        tank = tk;
        matrix = new Matrix();
        // Set tank object position to start tutorial
        tank.getPosition().x = 50;
        tank.getPosition().y = 50;

        setColor();
        playerObj.player.getTank().decrementShield();
        shieldHit = playerObj.player.getTank().getShieldLeft()-1;
    }

    public void draw() {
        setColor();
        matrix.reset();
        matrix.postTranslate(-tankBaseScaled.getWidth()/2.f, -tankBaseScaled.getHeight()/2.f);
        matrix.postRotate(tank.getMoveState().angle);
        matrix.postTranslate(activity.normX(tank.getPosition().x), activity.normY(tank.getPosition().y));

        // Draw shield
        if( playerObj.player.getTank().getShieldLeft() > 0 ) {
            if (playerObj.player.getTank().getShieldLeft() == shieldHit) {
                activity.paint.setColor(Color.argb(100, 255, 255, 255));
                shieldHit--;
            } else {
                activity.paint.setColor(Color.argb(100, 100, 129, 0));
            }
            activity.canvas.drawCircle(activity.normX(tank.getPosition().x), activity.normY(tank.getPosition().y),
                    tankBaseScaled.getWidth(), activity.paint);
        }
        // Draw scaled base
        activity.canvas.drawBitmap(tankBaseScaled, matrix, null);

        matrix.reset();
        matrix.postTranslate(-tankTurretScaled.getWidth()/2.f, -tankTurretScaled.getHeight()/2.f);
        matrix.postRotate(tank.getTurretAngle());
        matrix.postTranslate(activity.normX(tank.getPosition().x), activity.normY(tank.getPosition().y));
        //matrix.postTranslate(tankBaseScaled.getWidth()/2.f, tankBaseScaled.getHeight()/2.f);

        // Draw scaled turret
        activity.canvas.drawBitmap(tankTurretScaled, matrix, null);
//        gameState.activity.canvas.drawBitmap( image, position.x, position.y, gameState.activity.paint );

    }

    public void update(float dt) {
        float moveSpeed = speed * (float)dt / 1000.f;
        // Horizontal movement
        tank.getPosition().x += tank.getMoveState().h * moveSpeed;
        // Check if position x is off screen
        // Will need to use map class for this check

        if(tank.getPosition().x + tankBase.getWidth()/2.f > activity.mapWidth)
            tank.getPosition().x = activity.mapWidth - tankBase.getWidth()/2.f;
        if(tank.getPosition().x - tankBase.getWidth()/2 < 0.f)
            tank.getPosition().x = 0.f + tankBase.getWidth()/2;

        // Vertical movement
        tank.getPosition().y += tank.getMoveState().v * moveSpeed;
        // Check if position x is off screen
        // Will need to use map class for this check
        if(tank.getPosition().y + tankBase.getHeight()/2.f > activity.mapHeight)
            tank.getPosition().y = activity.mapHeight - tankBase.getHeight()/2.f;
        if(tank.getPosition().y - tankBase.getHeight()/2.f < 0.f )
            tank.getPosition().y = 0.f + tankBase.getHeight()/2.f;

        wallCollision();
        //playerObj.bulletDetection();
    }

    public void setColor()
    {
        switch(tank.getColor()) {
            case RED:
                tankTurret = activity.loadBitmap(R.drawable.tank_turretp1);
                break;
            case GREEN:
                tankTurret = activity.loadBitmap(R.drawable.tank_turretp2);
                break;
            case BLUE:
                tankTurret = activity.loadBitmap(R.drawable.tank_turretp3);
                break;
            case YELLOW:
                tankTurret = activity.loadBitmap(R.drawable.tank_turretp4);
                break;
        }
        tankTurretScaled = activity.normBitmap(tankTurret);
    }

    // Handles collisions between tanks and walls
    public void wallCollision() {
        //float temp = (float) Math.sin(tank.moveState.angle + Math.PI/4);
        //temp *= Math.sqrt(Math.pow(tankBase.getWidth()/2.f, 2) + (Math.pow(tankBase.getHeight()/2.f, 2))) - tankBase.getWidth();

        for(int i=0; i<gameState.map.wallHArray.length; i++){

            Bitmap wallHArrayScaled = gameState.map.wallHArray[i];
            float wallHXPosScaled = gameState.map.wallHXPos[i];
            float wallHYPosScaled = gameState.map.wallHYPos[i];

            //Collision on right side of tankObj for horizontal walls
            if(isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth()/2.f), Math.round(tank.getPosition().y - tankBase.getHeight()/2.f),
                    wallHArrayScaled, Math.round(wallHXPosScaled), Math.round(wallHYPosScaled)) &&
                    (tank.getPosition().x + tankBase.getWidth() / 2.f > wallHXPosScaled  && tank.getPosition().x + tankBase.getWidth()/ 2.f < wallHXPosScaled + wallHArrayScaled.getWidth()/30.f)){
                tank.getPosition().x = wallHXPosScaled - tankBase.getWidth() / 2.f ;
            }
            // Collision on left side of tankObj for horizontal walls
            if(isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth()/2.f), Math.round(tank.getPosition().y - tankBase.getHeight()/2.f),
                    wallHArrayScaled, Math.round(wallHXPosScaled), Math.round(wallHYPosScaled))  &&
                    (tank.getPosition().x - tankBase.getWidth() / 2.f < wallHXPosScaled + wallHArrayScaled.getWidth() && tank.getPosition().x - tankBase.getWidth() / 2.f >
                            wallHXPosScaled + wallHArrayScaled.getWidth() - wallHArrayScaled.getWidth()/30.f)){
                tank.getPosition().x = wallHXPosScaled+ wallHArrayScaled.getWidth()+ tankBase.getWidth() / 2.f;
            }
            // Collision on bottom of tankObj for horizontal walls
            if(isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth()/2.f), Math.round(tank.getPosition(). y- tankBase.getHeight()/2.f),
                    wallHArrayScaled, Math.round(wallHXPosScaled), Math.round(wallHYPosScaled))  &&
                    (tank.getPosition().y + tankBase.getHeight() / 2.f> wallHYPosScaled && tank.getPosition().y + tankBase.getHeight() / 2.f < wallHYPosScaled + wallHArrayScaled.getHeight()/2.f)){
                tank.getPosition().y =wallHYPosScaled- tankBase.getHeight() / 2.f ;
            }
            // Collision on top of tankObj for horizontal walls
            if(isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth()/2.f), Math.round(tank.getPosition().y - tankBase.getHeight()/2.f),
                    wallHArrayScaled, Math.round(wallHXPosScaled), Math.round(wallHYPosScaled)) &&
                    (tank.getPosition().y - tankBase.getHeight() / 2.f < wallHYPosScaled+ wallHArrayScaled.getHeight() && tank.getPosition().y - tankBase.getHeight() / 2.f >
                            wallHYPosScaled + wallHArrayScaled.getHeight() - wallHArrayScaled.getHeight()/2.f)) {
                tank.getPosition().y = wallHYPosScaled+ wallHArrayScaled.getHeight() + tankBase.getHeight() / 2.f;
            }
        }

        for(int i=0; i<gameState.map.wallVArray.length; i++) {

            Bitmap wallVArrayScaled = gameState.map.wallVArray[i];
            float wallVXPosScaled = gameState.map.wallVXPos[i];
            float wallVYPosScaled = gameState.map.wallVYPos[i];
            //Collision on right side of tankObj for vertical walls
            if (isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth() / 2.f), Math.round(tank.getPosition().y - tankBase.getHeight() / 2.f),
                    wallVArrayScaled, Math.round(wallVXPosScaled), Math.round(wallVYPosScaled)) &&
                    (tank.getPosition().x + tankBase.getWidth() / 2.f> wallVXPosScaled && tank.getPosition().x + tankBase.getWidth() / 2.f< wallVXPosScaled + wallVArrayScaled.getWidth() / 2.f)) {
                tank.getPosition().x = wallVXPosScaled - tankBase.getWidth() / 2.f;
            }
            // Collision on left side of tankObj for vertical walls
            if (isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth() / 2.f), Math.round(tank.getPosition().y - tankBase.getHeight() / 2.f),
                    wallVArrayScaled, Math.round(wallVXPosScaled), Math.round(wallVYPosScaled)) &&
                    (tank.getPosition().x - tankBase.getWidth() / 2.f  < wallVXPosScaled + wallVArrayScaled.getWidth() && tank.getPosition().x - tankBase.getWidth() / 2.f >
                            wallVXPosScaled + wallVArrayScaled.getWidth() - wallVArrayScaled.getWidth() / 2.f)) {
                tank.getPosition().x = wallVXPosScaled + wallVArrayScaled.getWidth() + tankBase.getWidth() / 2.f;
            }
            // Collision on bottom of tankObj for vertical walls
            if (isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth() / 2.f), Math.round(tank.getPosition().y - tankBase.getHeight() / 2.f),
                    wallVArrayScaled, Math.round(wallVXPosScaled), Math.round(wallVYPosScaled)) &&
                    (tank.getPosition().y + tankBase.getHeight() / 2.f > wallVYPosScaled && tank.getPosition().y + tankBase.getHeight() / 2.f < wallVYPosScaled + wallVArrayScaled.getHeight() / 20.f)) {
                tank.getPosition().y = wallVYPosScaled - tankBase.getHeight() / 2.f;
            }
            // Collision on top of tankObj for vertical walls
            if (isCollisionDetected(tankBase, Math.round(tank.getPosition().x - tankBase.getWidth() / 2.f), Math.round(tank.getPosition().y - tankBase.getHeight() / 2.f),
                    wallVArrayScaled, Math.round(wallVXPosScaled), Math.round(wallVYPosScaled)) &&
                    (tank.getPosition().y - tankBase.getHeight() / 2.f< wallVYPosScaled + wallVArrayScaled.getHeight() && tank.getPosition().y - tankBase.getHeight() / 2.f >
                            wallVYPosScaled + wallVArrayScaled.getHeight() - wallVArrayScaled.getHeight() / 20.f)) {
                tank.getPosition().y = wallVYPosScaled + wallVArrayScaled.getHeight() + tankBase.getHeight() / 2.f;
            }
        }
        return;
    }


    // Checks for collision between two bitmaps
    public boolean isCollisionDetected(Bitmap bitmap1, int x1, int y1,
                                              Bitmap bitmap2, int x2, int y2) {

        Rect bounds1 = new Rect(x1, y1, x1+bitmap1.getWidth(), y1+bitmap1.getHeight());
        Rect bounds2 = new Rect(x2, y2, x2+bitmap2.getWidth(), y2+bitmap2.getHeight());

        if (Rect.intersects(bounds1, bounds2)) {
            Rect collisionBounds = getCollisionBounds(bounds1, bounds2);
            for (int i = collisionBounds.left; i < collisionBounds.right; i++) {
                for (int j = collisionBounds.top; j < collisionBounds.bottom; j++) {
                    int bitmap1Pixel = bitmap1.getPixel(i-x1, j-y1);
                    int bitmap2Pixel = bitmap2.getPixel(i-x2, j-y2);
                    if (isFilled(bitmap1Pixel) && isFilled(bitmap2Pixel)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private static Rect getCollisionBounds(Rect rect1, Rect rect2) {
        int left = (int) Math.max(rect1.left, rect2.left);
        int top = (int) Math.max(rect1.top, rect2.top);
        int right = (int) Math.min(rect1.right, rect2.right);
        int bottom = (int) Math.min(rect1.bottom, rect2.bottom);
        return new Rect(left, top, right, bottom);
    }


    private static boolean isFilled(int pixel) {
        return pixel != Color.TRANSPARENT;
    }
}
