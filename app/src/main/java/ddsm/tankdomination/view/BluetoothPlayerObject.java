package ddsm.tankdomination.view;

import java.util.Set;

import ddsm.tankdomination.GameState;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.Position;

import java.util.Map;

public class BluetoothPlayerObject extends PlayerObject {
    private Set<String> keys;


    public BluetoothPlayerObject(GameState gs, Player plr) {
        super(gs, plr);
        id = player.getUserId();
    }

    public void draw() {
        tankObj.draw();
        for(BulletObject b : bullets.values())
            b.draw();
    }

    public void update(long dt) {
        keys = bullets.keySet();

        if (player.getTank().getAlive()) {
            // Update tankObj
            tankObj.update(dt);
        }

        // Adds bullets if model player contains more bullets
        for(Bullet b: player.getBullets()){
            if(!keys.contains(b.getBulletId())){
                addBullet(b);
            }
        }

        // List of bullets to be deleted
        bulletsToRemove.clear();

        for( BulletObject b : bullets.values() ) {
            if( b.offScreen() )
                bulletsToRemove.add(b);
            else
                b.update(dt);
        }

        // Remove bullets in list
        for (BulletObject b : bulletsToRemove) {
            removeBullet(b);
        }
    }
    public void addBullet(float x, float y, float angle) {
        String bulletId = "b"+player.getUserId().substring(1,3)+player.getBulletIdCounter();
        String userId = player.getUserId();
        Bullet bullet = new Bullet(bulletId,userId,new Position(x,y),angle, player.getColor());
        player.addBullet(bullet);
        bullets.put(bulletId, new BulletObject(gameState, bullet));
        gameState.sendBullet(bullet);
        numBullet++;
    }

    public void addBullet(Bullet b){
        bullets.put(b.getBulletId(), new BulletObject(gameState, b));
        numBullet++;
    }
}
