package ddsm.tankdomination.view;

import android.graphics.Paint;

import java.util.LinkedList;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.model.*;

/**
 * Button class for menus and interfaces
 */

public class Button {
    GameActivity activity;
    String[] text;
    float halfLength;
    Position pos;
    Position size;
    float textSize;
    int backColor;
    int textColor;

    public Button(GameActivity a, String t, float x, float y, float width, float height, float ts, int bcol, int tcol) {
        activity = a;
        pos = new Position();
        size = new Position();
        text = t.split("_"); // Text that appears on the button. split based on '_' character for new
        halfLength = (float)text.length/2.f - 0.5f;
        pos.x = activity.normX(x); // X coordinate
        pos.y = activity.normY(y); // Y coordinate
        size.x = activity.norm(width); // Width in phone scale
        size.y = activity.norm(height); // Height in phone scale
        textSize = activity.norm(ts); // Text size in phone scale
        backColor = bcol; // Background color
        textColor = tcol; // Text color
    }


    public void draw() {
        float half_x = size.x/2.f;
        float half_y = size.y/2.f;
        // Draw Buttons
        activity.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        activity.paint.setColor(backColor);
        activity.canvas.drawRect(
                pos.x - half_x, pos.y - half_y,
                pos.x + half_x, pos.y + half_y,
                activity.paint);

        activity.paint.setColor(textColor);
        activity.paint.setTextAlign(Paint.Align.CENTER);
        activity.paint.setTextSize(textSize);
        // Draw text
        for (int i = 0; i < text.length; i++) {
            float lineOff = ((float)i - halfLength)*textSize + 0.40f * textSize;
            activity.canvas.drawText(
                    text[i],
                    pos.x,
                    pos.y + lineOff,
                    activity.paint);
        }
    }

    public boolean isPressed(float x, float y) {
        float half_x = size.x/2.f;
        float half_y = size.y/2.f;
        return (x >= pos.x - half_x &&
                x <= pos.x + half_x &&
                y >= pos.y - half_y &&
                y <= pos.y + half_y);
    }

    public void setText(String t) {
        text = t.split("_"); // Text that appears on the button. split based on '_' character for new
        halfLength = (float)text.length/2.f - 0.5f;
    }

    public void setBackground(int bcol) { backColor = bcol; }
}
