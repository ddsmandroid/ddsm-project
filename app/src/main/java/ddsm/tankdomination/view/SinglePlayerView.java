package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.GameState;
import ddsm.tankdomination.GameStateType;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.PlayerColor;
import ddsm.tankdomination.model.Position;
import ddsm.tankdomination.model.Tank;

import static ddsm.tankdomination.model.PlayerColor.BLUE;
import static ddsm.tankdomination.model.PlayerColor.RED;
import static ddsm.tankdomination.model.PlayerColor.YELLOW;

/**
 *
 *
 */
public class SinglePlayerView extends GameState {
    PauseView pauseView;
    static final boolean godMode = false;
    PlayerObject playerObject;
    LinkedList<BotObject> bots;
    LinkedList<BotObject> botsToRemove;
    Bitmap explosion;
    boolean rightTouch, leftTouch;
    boolean showOverlay;
    boolean isReady = false;
    long timeElapsed = 0;
    long timePerBot = 20000;
    final Position[] spawnLoc;
    boolean newRound;
    int round;
    int score;
    int maxBot = 7;
    int botNum = 0;

    public SinglePlayerView(GameActivity a ) {
        activity = a;
        gameStateType = GameStateType.SINGLE;
        pauseView = new PauseView(this);

        // Add players
        players = new TreeMap<String,PlayerObject>();
        Player player = new Player("player1","player1", PlayerColor.YELLOW);
        playerObject = new PlayerObject(this, player);
        players.put(player.getUserId(), playerObject);

        // Create bots list
        bots = new LinkedList<BotObject>();
        botsToRemove = new LinkedList<BotObject>();

        // Configure spawn locations
        // Todo move to map since this will be map dependant
        int outterX = 100;
        int outterY = 50;
        int innerX = 300;
        int innerY = 225;
        spawnLoc = new Position[8];
        spawnLoc[0] = new Position(outterX, outterY);
        spawnLoc[1] = new Position(activity.mapWidth - outterX, outterY);
        spawnLoc[2] = new Position(outterX, activity.mapHeight - outterY);
        spawnLoc[3] = new Position(activity.mapWidth - outterX, activity.mapHeight - outterY);
        spawnLoc[4] = new Position(innerX, innerY);
        spawnLoc[5] = new Position(activity.mapWidth - innerX, innerY);
        spawnLoc[6] = new Position(innerX, activity.mapHeight - innerY);
        spawnLoc[7] = new Position(activity.mapWidth - innerX, activity.mapHeight - innerY);

        controller = new Controller(this);
        map = new Map(this);
        playing = false;
        paused = false;
        newRound = true;
        round = 0; // Gets incremented before new round
        score = 0;

        explosion = activity.normBitmap(activity.loadBitmap(R.drawable.explosion));

        mediaPlayer = MediaPlayer.create(activity.getContext(), R.raw.bensound_epic);
        if( activity.musicOn ) {
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }
    }


    public void update(long deltaTime) {
        if (!playing || paused) return;

        timeElapsed += deltaTime;

        // Update playerObject
        playerObject.update(deltaTime);

        // God mode for debugging
        if (godMode) playerObject.tankObj.tank.setAlive(true);

        // Check if game is over
        playing = playerObject.player.getTank().getAlive();

        // Update comp
        botsToRemove.clear();
        int i = 0;
        for(BotObject bot : bots) {
            if (i++ == maxBot) break;
            bot.update(deltaTime);
            if (!bot.player.getTank().getAlive()) {
                botsToRemove.add(bot);
                score += 100 * timePerBot / bot.timeAlive;
            }
        }

        // Remove bots from linked list
        for(BotObject bot : botsToRemove) {
            bots.remove(bot);
            players.remove(bot.id);
        }

        // Check if bots list is empty
        if(bots.isEmpty()) {
            newRound = true;
            // Remove players bullets
            playerObject.bullets.clear();
            playerObject.numBullet = 0;
            // Reset shields
            playerObject.tankObj.tank.setShieldLeft(Tank.maxShield);
        }
    }

    public void draw() {
        // Display countdown for new round
        if (newRound) drawNewRound();

        if( activity.holder.getSurface().isValid() ) {
            // Lock the canvas to draw
            activity.canvas = activity.holder.lockCanvas();

            //@TODO: Draw map instead of solid colour
            // BACK GROUND COLOR
            activity.canvas.drawColor(Color.argb(255, 0, 0, 0));

            map.draw();

            if (activity.debug) {
                // Choose brush color for Drawing
                activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                activity.paint.setTextSize(activity.norm(60.f));

                // Display current FPS on screen
                activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
            }

            playerObject.draw();
            if (!playerObject.player.getTank().getAlive()) activity.canvas.drawBitmap(explosion, playerObject.tankObj.matrix, null);

            // Draw bots
            int i = 0;
            for (BotObject bot : bots) {
                if (i++ == maxBot) break;
                bot.draw();
            }

            if( playing ) {
                if (paused) pauseView.draw();
                // Draw controller overlay
                controller.draw();

            } else {
                activity.paint.setTextSize(activity.norm(100.f));
                activity.drawText("Game Over", activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f - 50), activity.paint);
                activity.drawText("Level " + round + " Score: " + score, activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f + 50), activity.paint);
                activity.paint.setTextSize(activity.norm(80.f));
                activity.drawText("Click to return to main menu.", activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f+120), activity.paint);
            }

            // Draw everything to the screen and unlock surface
            activity.holder.unlockCanvasAndPost(activity.canvas);
        }
    }

    public void drawNewRound() {
        // Increment round number
        round++;
        // Configure player position
        int currSpawnID = (int)Math.floor(Math.random() * 8);
        playerObject.player.getTank().setPosition(spawnLoc[currSpawnID]);

        // Spawn bots
        bots.clear();
        int botSpawnID;
        for (int i = 0; i < round; i++) {
            while (true) {
                botSpawnID = (int) Math.floor(Math.random() * 8);
                while (botSpawnID == currSpawnID)
                    botSpawnID = (int) Math.floor(Math.random() * 8);

                // Make sure bots don't share spawn location for first set of bots
                if (i >= maxBot) break;
                boolean unique = true;
                for(BotObject bot : bots) {
                    if (bot.spawnID == botSpawnID)
                        unique = false;
                }
                if (unique) break;
            }


            String botName = String.valueOf(botNum);
            botNum++;
            BotObject bot = new BotObject(this, new Player(botName, "Bot #" + botName, PlayerColor.RED), playerObject);
            bot.spawnID = botSpawnID;
            bot.player.getTank().setPosition(spawnLoc[bot.spawnID]);
            bots.add(bot);
            // Add to players
            players.put(bot.id, bot);
        }

        // Set bools
        newRound = false;
        playing = true;

        float totalCountDown = 4.f;
        float roundCountDown = totalCountDown;
        long countDownStartTime = System.currentTimeMillis();
        while (roundCountDown >= 0.f) {
            if( activity.holder.getSurface().isValid() ) {
                // Lock the canvas to draw
                activity.canvas = activity.holder.lockCanvas();

                // BACK GROUND COLOR
                activity.canvas.drawColor(Color.argb(255, 0, 0, 0));

                map.draw();

                if (activity.debug) {
                    // Choose brush color for Drawing
                    activity.paint.setColor( Color.argb( 255, 249, 129, 0) );
                    activity.paint.setTextSize(activity.norm(60.f));

                    // Display current FPS on screen
                    activity.canvas.drawText( "FPS:" + activity.fps, activity.normX(120.f), activity.normY(40.f), activity.paint );
                }

                playerObject.draw();

                // Draw bots
                int i = 0;
                for (BotObject bot : bots) {
                    if (i++ == maxBot) break;
                    bot.draw();
                }

                // Draw level
                activity.paint.setTextSize(activity.norm(100.f));
                activity.drawText("Level " + round, activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f - 200.f), activity.paint);

                // Draw Count down time
                int number = (int)Math.floor(roundCountDown);
                float alpha = (float)number - roundCountDown;

                activity.paint.setTextSize(activity.norm(100.f));
                activity.drawText(String.valueOf(number), activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f), activity.paint, 1.f - alpha);

                activity.paint.setTextSize(activity.norm(100.f));
                activity.drawText("Score " + score, activity.normX(activity.mapWidth/2.f), activity.normY(activity.mapHeight/2.f + 200.f), activity.paint);

                // Draw everything to the screen and unlock surface
                activity.holder.unlockCanvasAndPost(activity.canvas);

                // Sleep and set time
                try {
                    Thread.sleep(20, 0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                roundCountDown = totalCountDown - (float)(System.currentTimeMillis() - countDownStartTime)/1000.f;
                //System.out.println(roundCountDown);
            }
        }
        // Reset activity update time due to loop take over
        activity.resetTime();
    }

    public boolean touchEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
        if( action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
            int pointerIndex = MotionEventCompat.getActionIndex(event);
            float x = MotionEventCompat.getX(event, pointerIndex);
            float y = MotionEventCompat.getY(event, pointerIndex);
            if (paused && playing) pauseView.touchEvent(x, y);
            else if (!playing) {
                this.mediaPlayer.stop();
                if (action == MotionEvent.ACTION_DOWN)
                    activity.setGameState(GameStateType.MENU);
            }
        }

        return controller.touchEvent(event);
    }

    public LinkedList<PlayerObject> getPlayersList() {
        LinkedList<PlayerObject> pList = new LinkedList<PlayerObject>();
        // Add players to linkedList
        pList.add(playerObject);
        int i = 0;
        for(BotObject bot : bots) {
            if (i++ == maxBot) break;
            pList.add(bot);
        }

        return pList;
    }

    @Override
    public void sendBullet(Bullet bullet) {

    }

    @Override
    public void sendHit(String type, String id, String ownerId) {

    }
}
