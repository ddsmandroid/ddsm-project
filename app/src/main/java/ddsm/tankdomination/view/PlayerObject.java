package ddsm.tankdomination.view;

import android.media.AudioManager;
import android.media.SoundPool;

import java.util.LinkedList;
import java.util.TreeMap;

import ddsm.tankdomination.GameState;
import ddsm.tankdomination.R;
import ddsm.tankdomination.model.Bullet;
import ddsm.tankdomination.model.Player;
import ddsm.tankdomination.model.Position;

import static android.content.Context.AUDIO_SERVICE;
import static ddsm.tankdomination.GameState.activity;

;

public class PlayerObject {
    GameState gameState;
    String id;
    Player player;
    TankObject tankObj;

    LinkedList<BulletObject> bulletsToRemove = new LinkedList<BulletObject>();
    TreeMap<String,BulletObject> bullets;
    int numBullet = 0;
    int maxBullet = 5;

    // Sound components
    final SoundPool         soundPool;
    final int               fireSound;
    final int               explosionSound;
    boolean                 soundLoaded = false;

    // BulletObject Constants
    long timeElapsed = 0;
    long waitMillis = 500; // How long to wait before firing another bulletMap

    public PlayerObject(GameState gs, Player plr) {
        player = plr;
        id = plr.getUserId();
        gameState = gs;

        // Set tankObj object
        tankObj = new TankObject(gameState, this, player.getTank());

        bullets = new TreeMap<>();
        timeElapsed = System.currentTimeMillis();

        player.getTank().setAlive(true);

        // Initialize the sounds
        soundPool = new SoundPool( 10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {

            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                soundLoaded = true;
            }
        });
        fireSound = soundPool.load( activity.getContext(), R.raw.tank_firing, 2);
        explosionSound = soundPool.load( activity.getContext(), R.raw.tank_explosion, 1 );
    }

    public void draw() {
        tankObj.draw();
        for(BulletObject b : bullets.values())
            b.draw();
    }

    public void getControls() {
        // Get Move State from Controller
        player.getTank().getMoveState().h = gameState.controller.leftMoveState.h;
        player.getTank().getMoveState().v = gameState.controller.leftMoveState.v;
        player.getTank().getMoveState().angle = gameState.controller.leftMoveState.angle;
        player.getTank().setTurretAngle(gameState.controller.rightMoveState.angle);
    }

    public void getTurretFire(long dt) {
        // Check if turret if fired
        timeElapsed += dt;
        if( gameState.controller.rightPress &&
                numBullet != maxBullet &&
                timeElapsed  > waitMillis ) {
            timeElapsed = 0;
            addBullet(player.getTank().getPosition().x,
                    player.getTank().getPosition().y,
                    player.getTank().getTurretAngle());
        }
    }

    public void update(long dt) {
        if (player.getTank().getAlive()) {
            // Get controls
            getControls();

            // Update tankObj
            tankObj.update(dt);

            // Get turret fire
            getTurretFire(dt);
        }

        // List of bullets to be deleted
       bulletsToRemove.clear();

        for( BulletObject b : bullets.values() ) {
            if( b.offScreen() )
                bulletsToRemove.add(b);
            else
                b.update(dt);
        }

        // Remove bullets in list
        for (BulletObject b : bulletsToRemove) {
            removeBullet(b);
        }

        // Bullet detection
        gotHit( bulletDetection() );

        tankDetection();
    }

    public void addBullet(float x, float y, float angle) {
        String bulletId = "b"+player.getUserId().substring(1,3)+player.getBulletIdCounter();
        String userId = player.getUserId();
        Bullet bullet = new Bullet(bulletId,userId,new Position(x,y),angle, player.getColor());
        player.addBullet(bullet);
        bullets.put(bulletId, new BulletObject(gameState, bullet));
        gameState.sendBullet(bullet);
        numBullet++;
        playSound( fireSound );
    }

    public void addBullet(Bullet b){
        bullets.put(b.getBulletId(), new BulletObject(gameState, b));
        numBullet++;
        playSound(fireSound);
    }

    public void removeBullet( BulletObject bullet ) {
        player.removeBullet(bullet.bullet);
        bullets.remove(bullet.bullet.getBulletId());
        numBullet--;
    }

    /*
     * Check if the computer bullet hit the playerObject
     * Return who this playerObject got hit by
     */
    public PlayerObject bulletDetection() {
        for (TreeMap.Entry<String,PlayerObject> entry : gameState.players.entrySet()) {
            if (entry.getKey() == id) continue; // Skip own bullet detection
            PlayerObject p = entry.getValue();

            // Check playerObject p's bullets
            for( BulletObject aBullet : p.bullets.values() ) {
                if( this.player.getTank().getShieldLeft() > 0 ) {

                    if( (aBullet.bullet.getPosition().x > tankObj.tank.getPosition().x - tankObj.tankTurret.getWidth()  && aBullet.bullet.getPosition().x < tankObj.tank.getPosition().x + tankObj.tankTurret.getWidth()) &&
                            (aBullet.bullet.getPosition().y > tankObj.tank.getPosition().y - tankObj.tankTurret.getHeight()  && aBullet.bullet.getPosition().y < tankObj.tank.getPosition().y + tankObj.tankTurret.getHeight())) {
                        p.removeBullet(aBullet);
                        return p;
                    }
                }
                else {
                    if( (aBullet.bullet.getPosition().x > tankObj.tank.getPosition().x - tankObj.tankTurret.getWidth()/2  && aBullet.bullet.getPosition().x < tankObj.tank.getPosition().x + tankObj.tankTurret.getWidth()/2.f) &&
                        (aBullet.bullet.getPosition().y > tankObj.tank.getPosition().y - tankObj.tankTurret.getHeight()/2  && aBullet.bullet.getPosition().y < tankObj.tank.getPosition().y + tankObj.tankTurret.getHeight()/2.f)) {
                        p.removeBullet(aBullet);

                        return p;
                    }
                }
            }
        }
        return null;
    }

    // Tank on tank collision
    public void tankDetection() {

        float tankHalfWidth = tankObj.tankBase.getWidth() / 2.f;
        float tankHalfHeight = tankObj.tankBase.getHeight() / 2.f;
        for (TreeMap.Entry<String, PlayerObject> entry : gameState.players.entrySet()) {
            if (entry.getKey() == id) continue; // Skip own tank detection
            PlayerObject p = entry.getValue();

            // Check from right side of playerObject
            if(tankObj.isCollisionDetected(tankObj.tankBase, (int) tankObj.tank.getPosition().x, (int) tankObj.tank.getPosition().y,
                    p.tankObj.tankBase, (int) p.tankObj.tank.getPosition().x, (int) p.tankObj.tank.getPosition().y) &&
                    (tankObj.tank.getPosition().x + tankHalfWidth > p.tankObj.tank.getPosition().x -  tankHalfWidth && tankObj.tank.getPosition().x + tankHalfWidth
                            < p.tankObj.tank.getPosition().x + tankHalfWidth/3.f))
                tankObj.tank.getPosition().x = p.tankObj.tank.getPosition().x - tankHalfWidth*2 ;

            // Check from left side of playerObject
            if(tankObj.isCollisionDetected(tankObj.tankBase, (int) tankObj.tank.getPosition().x, (int) tankObj.tank.getPosition().y,
                    p.tankObj.tankBase, (int) p.tankObj.tank.getPosition().x, (int) p.tankObj.tank.getPosition().y) &&
                    (tankObj.tank.getPosition().x - tankHalfWidth < p.tankObj.tank.getPosition().x + tankHalfWidth && tankObj.tank.getPosition().x - tankHalfWidth
                            > p.tankObj.tank.getPosition().x + tankHalfWidth - tankHalfWidth/3.f))
                tankObj.tank.getPosition().x = p.tankObj.tank.getPosition().x + tankHalfWidth *2 ;

            // Check from bottom of playerObject
            if(tankObj.isCollisionDetected(tankObj.tankBase, (int) tankObj.tank.getPosition().x, (int) tankObj.tank.getPosition().y,
                    p.tankObj.tankBase, (int) p.tankObj.tank.getPosition().x, (int) p.tankObj.tank.getPosition().y) &&
                    (tankObj.tank.getPosition().y + tankHalfHeight > p.tankObj.tank.getPosition().y - tankHalfHeight && tankObj.tank.getPosition().y + tankHalfHeight
                            < p.tankObj.tank.getPosition().y + tankHalfHeight/3.f))
                tankObj.tank.getPosition().y = p.tankObj.tank.getPosition().y - tankHalfHeight *2 ;

            // Check from top of playerObject
            if(tankObj.isCollisionDetected(tankObj.tankBase, (int) tankObj.tank.getPosition().x, (int) tankObj.tank.getPosition().y,
                    p.tankObj.tankBase, (int) p.tankObj.tank.getPosition().x, (int) p.tankObj.tank.getPosition().y) &&
                    (tankObj.tank.getPosition().y - tankHalfHeight < p.tankObj.tank.getPosition().y + tankHalfHeight && tankObj.tank.getPosition().y - tankHalfHeight
                            > p.tankObj.tank.getPosition().y + tankHalfHeight - tankHalfHeight/3.f))
                tankObj.tank.getPosition().y = p.tankObj.tank.getPosition().y + tankHalfHeight *2 ;
        }
    }

    public Position getPosition() {
        return this.player.getTank().getPosition();
    }

    public float getTurretAngle() {
        return this.tankObj.tank.getTurretAngle();
    }

    public void playSound( int soundEffect  ) {
        if( soundLoaded && activity.effectsOn ) {
            // Getting the user sound settings
            AudioManager audioManager = (AudioManager) activity.getContext().getSystemService(AUDIO_SERVICE);
            float actualVolume = (float) audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = (float) audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            float volume = actualVolume / maxVolume;
            soundPool.play(soundEffect, volume, volume, 1, 0, 1f);
        }
    }

    /*
        If this playerObject got hit then reduce the shield or declare dead
        @TODO: Send message to others saying this playerObject is dead and who killed this playerObject
     */
    public void gotHit( PlayerObject killer ) {
        if( killer != null ) {
            this.player.getTank().decrementShield();
            if( this.player.getTank().getShieldLeft() < 0 ) {
                player.getTank().setAlive(false);
                playSound( explosionSound );
                //@TODO: Send Death message
                gameState.sendHit("bull",player.getTank().getTankId(), player.getUserId());
            }
        }
    }
}
