package ddsm.tankdomination.view;

import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.LinkedList;
import java.util.concurrent.Callable;

import ddsm.tankdomination.GameActivity;
import ddsm.tankdomination.model.*;

/**
 * Button class for menus and interfaces
 */

public class ButtonBitmap {
    GameActivity activity;
    Bitmap bitmapOn;
    Bitmap bitmapOff;
    Position pos;
    Position size;

    public ButtonBitmap(GameActivity a, Bitmap bon, Bitmap boff, float x, float y) {
        activity = a;
        bitmapOn = activity.normBitmap(bon);
        bitmapOff = activity.normBitmap(boff);
        pos = new Position();
        pos.x = activity.normX(x); // X coordinate
        pos.y = activity.normY(y); // Y coordinate
        size = new Position();
        size.x = bitmapOn.getWidth(); // Width in phone scale
        size.y = bitmapOff.getWidth(); // Height in phone scale
    }


    public void draw(boolean state) {
        float half_x = size.x/2.f;
        float half_y = size.y/2.f;
        // Draw Buttons
        if (state) {
            activity.canvas.drawBitmap(bitmapOn, pos.x - half_x, pos.y - half_y, activity.paint);
        } else {
            activity.canvas.drawBitmap(bitmapOff, pos.x - half_x, pos.y - half_y, activity.paint);
        }
    }

    public boolean isPressed(float x, float y) {
        float half_x = size.x/2.f;
        float half_y = size.y/2.f;
        return (x >= pos.x - half_x &&
                x <= pos.x + half_x &&
                y >= pos.y - half_y &&
                y <= pos.y + half_y);
    }
}
