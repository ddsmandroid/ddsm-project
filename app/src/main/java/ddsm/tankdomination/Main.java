package ddsm.tankdomination;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Main entry point to application
 */

public class Main extends AppCompatActivity {
    GameActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();

        // Hide the status bar
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility( uiOptions );

        activity = new GameActivity(this);
        setContentView(activity);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        activity.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        activity.pause();
    }

    @Override
    public void onBackPressed() {
        switch(activity.getGameStateType()) {
            case MENU:
                activity.exitGame();
                break;
            case SINGLE:
                activity.togglePause();
                break;
            default:
                activity.setGameState(GameStateType.MENU);
        }
    }
}
