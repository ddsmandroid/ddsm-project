package ddsm.tankdomination.model;

/**
 * Created by david on 2016-11-13.
 */

public class Position {
    public float x;
    public float y;

    public Position() {
        x = 0.f;
        y = 0.f;
    }

    public Position(float x1, float y1){
        x=x1;
        y=y1;
    }

    public Position(int x1, int y1){
        x = (float)x1;
        y= (float)y1;
    }

    public void set(Position p) {
        x = p.x;
        y = p.y;
    }

    public String toString() {
        return "X: " + x + ", Y: " + y;
    }
}
