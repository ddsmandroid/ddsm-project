package ddsm.tankdomination.model;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by david on 2016-11-15.
 */

public class Model {
    private TreeMap<String,Player> players;
    private ArrayList<String> keys = new ArrayList<>();
    private String userKey;
    private int numPlayers=0;

    public Model(String userId, String name, PlayerColor color){
        userKey = userId;
        players = new TreeMap<String,Player>();
        addPlayer(userId, name, color);
    }

    public void addPlayer(String userId, String name, PlayerColor color){
        players.put(userId, new Player(userId, name,color));
        keys.add(userId);
        numPlayers++;
    }

    public void addPlayer(Player player){
        if(!players.containsKey(player.getUserId())){
            players.put(player.getUserId(), player);
            keys.add(player.getUserId());
            numPlayers++;
        }
    }

    public void removePlayer(String userId){
        players.remove(userId);
        keys.remove(userId);
        numPlayers--;
    }

    public TreeMap<String, Player> getPlayers() {
        return players;
    }

    public String getUserId(){
        return userKey;
    }

    public Player getPlayer(String id){
        if(players.containsKey(id)){
            return players.get(id);
        }
        return null;
    }

    public int getNumPlayers(){
        return numPlayers;
    }

    public String toString(){
        String s = "User Key: "+userKey+ ", Number of Players: " + numPlayers +",\n";
        for(String key: keys){
            s += players.get(key).toString() +",\n";
        }
        return s;
    }
}
