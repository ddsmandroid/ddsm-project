package ddsm.tankdomination.model;

/**
 * Created by david on 2016-11-13.
 */

public class MoveState {
    public float h;
    public float v;
    public float angle;

    public MoveState() {
        h = 0.f;
        v = 0.f;
        angle = 0.f;
    }

    public MoveState(float h1, float v1, float angle1){
        h = h1;
        v = v1;
        angle = angle1;
    }

    public MoveState(int h1, int v1, int angle1){
        h = (float)h1;
        v = (float)v1;
        angle = (float)angle1;
    }

    public String toString() {
        return "H: " + h + ", V: " + v + ", Angle: " + angle;
    }
}
