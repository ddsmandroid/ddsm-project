package ddsm.tankdomination.model;

import android.graphics.Color;

/**
 * Created by david on 2016-11-14.
 */

public class Bullet implements Comparable{
    private String bulletId;
    private String ownerId;
    private Position position;
    private float angle;
    private Boolean isAlive;
    private PlayerColor color;
/*
    public Bullet (String bullet, String owner){
        bulletId = bullet;
        ownerId = owner;
        position = new Position();
        angle = 0;
        isAlive = true;
        this.color = PlayerColor.GREEN;
    }

    public Bullet(String bullet, String owner, PlayerColor color){
        bulletId = bullet;
        ownerId = owner;
        position = new Position();
        angle = 0;
        isAlive = true;
        this.color = color;
    }*/

    public Bullet(String i, String owner, Position pos, float ang, PlayerColor color){
        bulletId = i;
        ownerId = owner;
        position = pos;
        angle = ang;
        isAlive = true;
        this.color = color;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public void setBulletId(String bulletId) {
        this.bulletId = bulletId;
    }

    public String getBulletId() {
        return bulletId;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getAngle() {
        return angle;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setAlive(Boolean alive) {
        isAlive = alive;
    }

    public Boolean getAlive() {
        return isAlive;
    }

    public void setColor(PlayerColor color) {
        this.color = color;
    }

    public PlayerColor getColor() {
        return color;
    }

    public boolean equals(Object o) {
        return bulletId.equals(((Bullet)o).bulletId);
    }

    public int compareTo(Object o) {
        return bulletId.compareTo(((Bullet)o).bulletId);
    }

    public String toString() {
        return "Bullet ID: " + bulletId + ",Owner ID: " + ownerId + ", Position: " + position.toString() + ", Angle: " + angle + ", Is Alive: " + isAlive.toString() + ", Color: " + color.toString() +"\n";
    }
}
