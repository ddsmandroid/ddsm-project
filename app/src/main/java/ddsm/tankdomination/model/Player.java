package ddsm.tankdomination.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.SortedMap;
import java.util.TreeMap;

public class Player implements Comparable{
    private String userId;
    private String name;
    private int numBullets;
    private int bulletIdCounter;
    private Tank tank;
    private SortedMap<String,Bullet> bullets;
    private PlayerColor color;

    public Player(String id, String nm, PlayerColor color){
        userId = id;
        name = nm;
        this.color = color;

        String name = "tank";
        tank = new Tank("t"+id.substring(1),id, new Position(), new MoveState(), color);

        bullets = Collections.synchronizedSortedMap(new TreeMap<String,Bullet>());
        bulletIdCounter = 0;
    }

    public synchronized void updateTank(Tank t){
        tank.setMoveState(t.getMoveState());
        tank.setPosition(t.getPosition());
        tank.setAlive(t.getAlive());
        tank.setTurretAngle(t.getTurretAngle());
    }

    public synchronized void addBullet(Bullet bullet){
        if(!bullets.containsKey(bullet.getBulletId())){
            bullets.put(bullet.getBulletId(), bullet);
        }
        else {
            if(bullets.get(bullet.getBulletId()).getAlive()==false){
                bullets.put(bullet.getBulletId(), bullet);
            }
        }
        numBullets++;
        bulletIdCounter++;
        if(bulletIdCounter==10){
            bulletIdCounter =0;
        }
    }

    public synchronized void removeBullet(Bullet bullet){
        bullets.get(bullet.getBulletId()).setAlive(false);
        numBullets--;
    }

    public synchronized Tank getTank() {
        return tank;
    }

    public synchronized LinkedList<Bullet> getBullets() {
        LinkedList<Bullet> b = new LinkedList<>(bullets.values());
        return b;
    }

    public synchronized void setName(String name) {
        this.name = name;
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized void setUserId(String userId) {
        this.userId = userId;
    }

    public synchronized String getUserId() {
        return userId;
    }

    public synchronized void setNumBullets(int numBullets) {
        this.numBullets = numBullets;
    }

    public synchronized int getNumBullets(){
        return numBullets;
    }

    public synchronized void setBulletIdCounter(int bulletIdCounter) {
        this.bulletIdCounter = bulletIdCounter;
    }

    public synchronized int getBulletIdCounter(){
        return bulletIdCounter;
    }

    public synchronized void setColor(PlayerColor color) {
        this.color = color;
        tank.setColor(color);
    }

    public synchronized PlayerColor getColor() {
        return color;
    }

    public synchronized int compareTo(Object o) {
        return userId.compareTo(((Player)o).userId);
    }

    public synchronized boolean equals(Object o) {
        return super.equals(o);
    }

    public String toString() {
        String s = "Player ID: "+userId+", Name: " + name + ", Num Bullets: " + numBullets + " Bullet ID Counter: " + bulletIdCounter + ",\n";
        s += "" + tank.toString() +",\n";
        for(Bullet bullet: bullets.values()){
            s += bullet.toString() +",\n";
        }
        return s;
    }
}
