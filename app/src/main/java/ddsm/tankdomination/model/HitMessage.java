package ddsm.tankdomination.model;

/**
 * Created by David on 2016-11-16.
 */

public class HitMessage {
    public String id;
    public String ownerId;
    public String type;

    public HitMessage(String i,String oid, String tp){
        id = i;
        ownerId = oid;
        type = tp;
    }
}
