package ddsm.tankdomination.model;

import android.graphics.Color;

/**
 * Created by david on 2016-11-14.
 */

public class Tank implements Comparable{
    private String tankId;
    private String ownerId;
    private Position position;
    private MoveState moveState;
    private float turretAngle;
    private boolean alive;
    private int shieldLeft;
    private PlayerColor color;
    public static final int maxShield = 3;

    /*
    public Tank(String tank, String owner){
        tankId = tank;
        ownerId = owner;
        position = new Position();
        moveState = new MoveState();
        turretAngle = 0;
        alive = true;
        shieldLeft = maxShield;
        color = PlayerColor.BLUE;
    }*/

    public Tank(String tank, String owner, Position pos, MoveState move){
        tankId = tank;
        ownerId = owner;
        position = pos;
        moveState = move;
        turretAngle = 0.f;
        alive = true;
        shieldLeft = 3;
        color = PlayerColor.BLUE;
    }

    public Tank(String tank, String owner, Position pos, MoveState move, PlayerColor color){
        tankId = tank;
        ownerId = owner;
        position = pos;
        moveState = move;
        turretAngle = 0.f;
        alive = true;
        shieldLeft = 3;
        this.color = color;
    }

    public Tank(String tank, String owner, Position pos, MoveState move, float angle){
        tankId = tank;
        ownerId = owner;
        position = pos;
        moveState = move;
        turretAngle = angle;
        alive = true;
        shieldLeft = 3;
    }

    public void setTankId(String tankId) {
        this.tankId = tankId;
    }

    public String getTankId() {
        return tankId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setPosition(Position position) {
        this.position.set(position);
    }

    public Position getPosition() {
        return position;
    }

    public void setMoveState(MoveState moveState) {
        this.moveState = moveState;
    }

    public MoveState getMoveState() {
        return moveState;
    }

    public void setTurretAngle(float turretAngle) {
        this.turretAngle = turretAngle;
    }

    public float getTurretAngle() {
        return turretAngle;
    }

    public void setAlive(boolean live) {
        alive = live;
    }

    public boolean getAlive(){return alive;};

    public void incrementShield (){
        shieldLeft++;
    }

    public void decrementShield(){
        if(shieldLeft!=-1){
            shieldLeft--;
        }
    }

    public void setShieldLeft(int shieldLeft) {
        this.shieldLeft = shieldLeft;
    }

    public int getShieldLeft() {
        return shieldLeft;
    }

    public void setColor(PlayerColor color) {
        this.color = color;
    }

    public PlayerColor getColor() {
        return color;
    }

    public int compareTo(Object o) {
        return tankId.compareTo(((Tank)o).getTankId());
    }

    public boolean equals(Object o) {
        return tankId.equals(((Tank)o).getTankId());
    }

    public String toString() {
        String s = "Tank ID: " + tankId + ", Owner ID: " + ownerId + ", Position: " + position + ", ";
        s+= "Move State: " + moveState + ", Turret Angle: " + turretAngle + ", Alive: " + alive + ", Shield Left: " + shieldLeft + ",\n";
        return s;
    }
}
