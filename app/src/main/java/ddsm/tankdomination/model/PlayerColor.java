package ddsm.tankdomination.model;

/**
 * Created by david on 2016-11-21.
 */
public enum PlayerColor { RED, GREEN, BLUE, YELLOW }
